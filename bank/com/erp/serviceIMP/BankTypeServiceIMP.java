package com.erp.serviceIMP;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.erp.dao.BankTypeDAO;
import com.erp.entity.BankType;
import com.erp.service.BankTypeService;

/**
 * 银行类型service实现
 * 
 * @author zhangsy
 * @version 1.0
 */
//@Service("bankTypeService")
public class BankTypeServiceIMP implements BankTypeService{
	
	@Autowired
	private BankTypeDAO bankTypeDao;
	
	/**
	 * 获取所有的银行类型
	 * @return 银行类型集合
	 */
	public List<BankType> getBankTypes(){
		return bankTypeDao.getBankTypes();
	}
	
	/**
	 * 根据ID查询银行类型信息
	 * @param bankTypeId
	 * @return
	 */
	public BankType getBankTypeById(String bankTypeId){
		return bankTypeDao.getBankTypeById(bankTypeId);
	}
	
	/**
	 * 保存银行类型
	 * @param bankType
	 */
	public void saveBankType(BankType bankType) throws Exception{
		//待完善
	}
	
	/**
	 * 根据ID删除银行类型
	 * @param bankTypeId
	 */
	public void deleteBankTypeById(String bankTypeId) throws Exception{
		//待完善
	}
	
	/**
	 * 更新银行类型信息
	 * @param bankType
	 */
	public void updateBankType(BankType bankType) throws Exception{
		//待完善
	}
}

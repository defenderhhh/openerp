package com.erp.dao;

import java.util.List;

import com.erp.entity.BankType;

/**
 * 银行类型接口
 * 
 * @author zhangsy
 * @version 1.0
 */
public interface BankTypeDAO {
	
	/**
	 * 获取所有的银行类型
	 * @return 银行类型集合
	 */
	List<BankType> getBankTypes();
	
	/**
	 * 根据ID查询银行类型信息
	 * @param bankTypeId
	 * @return
	 */
	BankType getBankTypeById(String bankTypeId);
	
	/**
	 * 保存银行类型
	 * @param bankType
	 */
	void saveBankType(BankType bankType);
	
	/**
	 * 根据ID删除银行类型
	 * @param bankTypeId
	 */
	void deleteBankTypeById(String bankTypeId);
	
	/**
	 * 更新银行类型信息
	 * @param bankType
	 */
	void updateBankType(BankType bankType);
}

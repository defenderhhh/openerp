package com.erp.dao;



import java.util.List;

import com.erp.entity.Users;
import com.erp.form.ErpObject;


public interface UserDAO {
	
	/**
	 * 
	 * @param name用户名
	 * @return 根据用户名查找用户
	 */
	public List<ErpObject> findByName(String name);
	
	public List<Users> findAllUser();
	
	public List<Users> findUserByRole(ErpObject erpObject);
	
}

package com.erp.serviceIMP;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.erp.dao.UserDAO;
import com.erp.entity.Users;
import com.erp.form.ErpObject;
import com.erp.service.UserService;

@Service("userService")
public class UserServiceIMP implements UserService{

    @Autowired
    private UserDAO userMapper;
    
    public List<Users> findAllUser() {
        return (List<Users>)userMapper.findAllUser();
    }

    public List<Users> findUserByRole(ErpObject erpObject) {
        return (List<Users>)userMapper.findUserByRole(erpObject);
    }

}

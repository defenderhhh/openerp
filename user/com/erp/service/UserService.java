package com.erp.service;

import java.util.List;

import com.erp.entity.Users;
import com.erp.form.ErpObject;

public interface UserService {

    public List<Users> findAllUser();
    
    public List<Users> findUserByRole(ErpObject erpObject);
}

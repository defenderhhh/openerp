package com.erp.control;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.erp.entity.Roles;
import com.erp.entity.Users;
import com.erp.form.ErpObject;
import com.erp.service.RolesServiceDAO;
import com.erp.service.UserService;

/**
 * 功能名: 选择用户controler<br/>
 * 功能描述: 选择用户controler<br/>
 * Copyright: Copyright (c) 2013<br/>
 * 公司: 曙光云计算有限公司<br/>
 *
 * @author liuhai
 * @version 1.0.0
 */
@Controller
@RequestMapping("/selectUserController")
public class SelectUserController {

    @Autowired
    private UserService userService;
    
    @Autowired
    private RolesServiceDAO roleService;
    
    @RequestMapping(value="testIndex.html",method = RequestMethod.GET)
    public String testSelectUserPage(){
        return "testSelectUser.jsp";
    }
    
    
    @RequestMapping(value="index.html")
    public String selectUserPage(){
        return "selectUser.jsp";
    }
    
    @ResponseBody
    @RequestMapping(value="roleList.html",method = RequestMethod.POST)
    public List<Roles> getRoleList(){
        return roleService.findAllRole();
    }
    
    @ResponseBody
    @RequestMapping(value="userList.html",method = RequestMethod.POST)
    public List<Users> getUserListByRole(ErpObject erpObject){
        return userService.findUserByRole(erpObject);
    }
    
    
}

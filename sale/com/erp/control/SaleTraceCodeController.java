package com.erp.control;
import java.beans.PropertyEditorSupport;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.erp.entity.SaleTraceCode;
import com.erp.form.SaleTraceCodeForm;
import com.erp.service.SaleTraceCodeService;
import com.erp.util.Json;
/**
 * 
 * @author jerry
 * created on 2013-10-31 下午10:08:47
 */
@Controller
@RequestMapping("/saleTraceCodeController")
public class SaleTraceCodeController {
    @Autowired
    private SaleTraceCodeService saleTraceCodeService;
    
    /**
     * 功能: 跳转到销售跟踪码页面
     *
     * @return String
     */
    @RequestMapping(value="index.html",method = RequestMethod.GET)
    public String providerTypePage(){
        return "sale/saleTraceCode.jsp";
    }
    
    /**
     * 功能: 获取销售跟踪码列表，自动将List转化为json传到页面
     *
     * @return List<ProviderType>
     */
    @ResponseBody
    @RequestMapping(value="list.html",method = RequestMethod.POST)
    public List<SaleTraceCode> getProviderTypeList(){
        return saleTraceCodeService.findAll();
    }
    
    /**
     * 功能: 跳转到添加销售跟踪码页面
     *
     * @return String
     */
    @RequestMapping(value="saleTraceCodeAdd.html")
    public String providerTypeAddPage(){
        return "sale/saleTraceCodeAdd.jsp";
    }
    
    /**
     * 功能: 新增销售跟踪码
     * 
     * @return Json
     */
    @RequestMapping(value="save.html",method = RequestMethod.POST)
    @ResponseBody
    public Json save(SaleTraceCodeForm saleTraceCodeForm) {
        Json j = new Json();
        try {
        	saleTraceCodeForm.setCreate_Date(new Timestamp(System.currentTimeMillis()));
        	saleTraceCodeForm.setLastUpdateDate(saleTraceCodeForm.getCreate_Date());
        	saleTraceCodeService.save(saleTraceCodeForm);
            j.setSuccess(true);
            j.setMsg("添加成功！");
        } catch (Exception e) {
            j.setMsg(e.getMessage());
        }
        return j;
    }
    
    /**
     * 功能: 删除销售跟踪码
     * 
     * @param saleTraceCodeForm
     */
    @RequestMapping(value="delete.html", method = RequestMethod.POST)
    @ResponseBody
    public Json deleteById(SaleTraceCodeForm id) {
    	Json j = new Json();
        try{
        	saleTraceCodeService.deleteById((id));
            j.setMsg("删除成功！");
            j.setSuccess(true);
        }catch(Exception e){
            j.setMsg(e.getMessage());
        }
        return j;
    }
    
    @InitBinder
    public void initBinder(WebDataBinder binder) { 
    	binder.registerCustomEditor(Timestamp.class, new PropertyEditorSupport(){

			@Override
			public void setAsText(String text) throws IllegalArgumentException {
				// TODO Auto-generated method stub
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				Date date = null;
				try {
					date = sdf.parse(text);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
				}
				setValue(new Timestamp(date.getTime()));
			}
    	});
    }
}

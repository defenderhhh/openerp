package com.erp.serviceIMP;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.erp.dao.SaleTraceCodeDao;
import com.erp.entity.SaleTraceCode;
import com.erp.service.SaleTraceCodeService;
/**
 * 
 * @author jerry
 * created on 2013-10-31 下午10:01:47
 */
@Service("saleTraceCodeService")
public class SaleTraceCodeServiceImpl implements SaleTraceCodeService {
	@Autowired
	private SaleTraceCodeDao saleTraceCodeMapper;
	
	public List<SaleTraceCode> findAll() {
		// TODO Auto-generated method stub
		return saleTraceCodeMapper.findAll();
	}

	public void save(SaleTraceCode saleTraceCode) {
		// TODO Auto-generated method stub
		saleTraceCodeMapper.save(saleTraceCode);
	}

	public void deleteById(SaleTraceCode saleTraceCode) {
		// TODO Auto-generated method stub
		saleTraceCodeMapper.deleteById(saleTraceCode);
	}

}

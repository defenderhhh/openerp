package com.erp.service;

import java.util.List;

import com.erp.entity.SaleTraceCode;
/**
 * 
 * @author jerry
 * created on 2013-10-31 下午10:08:10
 */
public interface SaleTraceCodeService {
	 /**
     * 
     * 功能: 查询所有的saleTraceCode
     *
     * @return
     */
    List<SaleTraceCode> findAll();
    
    /**
     * 
     * 功能: 新增销售追踪代码
     *
     * @param saleTraceCode
     */
    void save(SaleTraceCode saleTraceCode);
    
    /**
     * 
     * 功能: 删除销售追踪代码
     *
     * @param id
     */
    void deleteById(SaleTraceCode saleTraceCode);
}

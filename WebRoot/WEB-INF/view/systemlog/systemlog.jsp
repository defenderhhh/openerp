<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    <title>系统日志</title>
    <jsp:include page="../common.jsp"></jsp:include>
    <script type="text/javascript">
        var $sl;
        var $grid;
         function initDataGrid(url){
        	$grid = $sl.datagrid({
                url : url,
                width : 'auto',
                height :  $(this).height(),
                pagination:true,
                rownumbers:true,
                border:false,
                singleSelect:true,
                checkOnSelect : false,
                selectOnCheck : false,
                striped:true,
                columns : [ [ {field : 'user_name',title : '操作者',width : parseInt($(this).width()*0.1),align : 'left',editor : "text"},
                              {field : 'description',title : '操作内容',width : parseInt($(this).width()*0.1)},
                              {field : 'enter_method_time',title : '操作时间',width : parseInt($(this).width()*0.1)},
                              {field : 'method_info',title : '方法信息',width : parseInt($(this).width()*0.30)},
                              {field : 'session_id',title : 'session',width : parseInt($(this).width()*0.15)},
                              {field : 'enter_ip',title : 'ip',width : parseInt($(this).width()*0.1)}
                              ] ],toolbar:'#tb'
            });
        }
        $(function() {
            $sl = $("#sl");
            var url = "${pageContext.request.contextPath}/systemLogController/list.html";
            initDataGrid(url);
        });
        
        //删除所有数据
        function delAll(){
				parent.$.messager.confirm('询问', '您是否要删除所有日志记录？', function(b) {
					if (b) {
						$.post('${pageContext.request.contextPath}/systemLogController/delAll.html', {
						}, function() {
							var url = "${pageContext.request.contextPath}/systemLogController/list.html";
							initDataGrid(url)
						});
					}
				});
		}
        
        //查询
        function query(){
            var startTime = $("#startTime").val();
            var endTime = $("#endTime").val();  
            var username = $("#username").val();
            var url = "${pageContext.request.contextPath}/systemLogController/query.html?end_time="+endTime+"&start_time="+startTime+"&user_name="+username;
            initDataGrid(url);
        }
  </script>
  </head>
  
  <body>
    <div id="tb" style="padding:2px 0">
    <form action="">
         <fieldset>
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td>开始时间：<input id="startTime" name="start_time" type="date" class="easyui-textbox easyui-validatebox"/> </td>
                    <td style="padding-left: 20px;">结束时间：<input id="endTime" name="end_time" type="date" class="easyui-textbox easyui-validatebox"/></td>
                    <td style="padding-left: 20px;">登陆者：<input id="username" name="user_name" type="text" class="easyui-textbox easyui-validatebox"/></td>
                    <td style="padding-left: 20px;"><a href="javascript:void(0);" icon="icon-search" class="easyui-linkbutton" plain="true" onclick="query();">查询</a></td>
                    <td style="padding-left: 20px;"><a href="javascript:void(0);" icon="icon-remove" class="easyui-linkbutton" plain="true" onclick="delAll();">删除全部</a></td>
                </tr>
            </table>
         </fieldset>
    </form>
    </div>
    <div>
	    <table id="sl" title="系统日志管理" ></table>
    </div>
  </body>
</html>

<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<link rel="stylesheet"
	href="<%=basePath %>resource/easyui/themes/default/easyui.css"
	type="text/css" charset="UTF-8"></link>
<link rel="stylesheet"
	href="<%=basePath %>resource/easyui/themes/icon.css" type="text/css"
	charset="UTF-8"></link>
<script type="text/javascript"
	src="<%=basePath %>resource/jquery-1.8.3.js" charset="UTF-8"></script>
<script type="text/javascript"
	src="<%=basePath %>resource/easyui/jquery.easyui.min.js"
	charset="UTF-8"></script>

<script type="text/javascript"
	src="<%=basePath %>resource/easyui/locale/easyui-lang-zh_CN.js"
	charset="UTF-8"></script>

<script type="text/javascript"
	src="<%=basePath %>resource/jquery.form.js" charset="UTF-8"></script>
<script type="text/javascript"
	src="<%=basePath %>resource/jqueryUtil.js" charset="UTF-8"></script>

<script>

	function ti(onmsg ){
			parent.$.messager.show({
									title : '提示',
									msg : onmsg,
									timeout : 1000 * 2
								});
		}
</script>
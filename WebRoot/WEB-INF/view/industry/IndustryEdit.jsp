<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript">
	$(function() {
		$("#form").form({
			url :"${pageContext.request.contextPath}/IndustryController/IndustryUpdate.html",
			onSubmit : function() {
				parent.$.messager.progress({
					title : '提示',
					text : '数据处理中，请稍后....'
				});
				var isValid = $(this).form('validate');
				if (!isValid) {
					parent.$.messager.progress('close');
				}
				return isValid;
			},
			success : function(result) {
				parent.$.messager.progress('close');
				result = $.parseJSON(result);
				if (result.success) {
					parent.$.modalDialog.openner.datagrid('reload');
					parent.$.modalDialog.handler.dialog('close');
					parent.$.messager.show({
						title : '提示',
						msg : result.msg,
						timeout : 1000 * 2
					});
				}else{
					parent.$.messager.show({
						title :  '提示',
						msg : result.msg,
						timeout : 1000 * 2
					});
				}
			}
		});
	});
	
</script>
<div class="easyui-layout" data-options="fit:true,border:false">
	<div data-options="region:'center',border:false" title="" style="overflow: hidden;padding: 10px;">
		<form id="form" method="post">
			<fieldset>
				<input name="id" id="id" value="${industry.id}"  type="hidden"/>
				<input name="addUserId" id="addUserId" value="${industry.addUserId}"  type="hidden"/>
				 <table>
					 <table>
                     <tr>
                        <th>名称</th>
                        <td><input name="name" id="name" value="${industry.name }" type="text" class="easyui-textbox easyui-validatebox" required="required"/></td>
                     </tr>
                         <tr>
                        <th>编号</th>
                        <td><input name="code" id="code" value="${industry.code }" type="text" class="easyui-textbox easyui-validatebox" required="required"/></td>
                     </tr>
                          <tr>
                        <th>类别</th>
                        <td><input name="type" id="type" value="${industry.type }" type="text" class="easyui-textbox easyui-validatebox" required="required"/></td>
                     </tr>
                 </table>
					
				 </table>
			</fieldset>
		</form>
	</div>
</div>

<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript">
	$(function() {
		$("#form")
				.form(
						{
							url : "${pageContext.request.contextPath}/saleTraceCodeController/save.html",
							onSubmit : function() {
								parent.$.messager.progress({
									title : '提示',
									text : '数据处理中，请稍后....'
								});
								var isValid = $(this).form('validate');
								if (!isValid) {
									parent.$.messager.progress('close');
								}
								return isValid;
							},
							sutypeess : function(result) {
								parent.$.messager.progress('close');
								result = $.parseJSON(result);
								if (result.sutypeess) {
									parent.$.modalDialog.openner
											.datagrid('reload');//之所以能在这里调用到parent.$.modalDialog.openner_datagrid这个对象，是因为user.jsp页面预定义好了
									parent.$.modalDialog.handler
											.dialog('close');
									parent.$.messager.show({
										title : '提示',
										msg : result.msg,
										timeout : 1000 * 2
									});
								} else {
									parent.$.messager.show({
										title : '提示',
										msg : result.msg,
										timeout : 1000 * 2
									});
								}
							}
						});
		
		$('#type').combo({
			editable:false
		});
		$('#sp').appendTo($('#type').combo('panel'));
		$('#sp input').click(function(){
			var v = $(this).val();
			var s = $(this).next('span').text();
			$('#type').combo('setValue', v).combo('setText', s).combo('hidePanel');
		});
	});
</script>
<div class="easyui-layout" data-options="fit:true,border:false">
	<div data-options="region:'center',border:false" title=""
		style="overflow: hidden; padding: 10px;">
		<form id="form" method="post">
			<fieldset>
				<table>
					<tr>
						<th>销售追踪码</th>
						<td><input name="marketCode" id="marketCode" type="text"
							class="easyui-textbox" /></td>
					</tr>
					<tr>
						<th>描述</th>
						<td><input name="description" id="description" type="text"
							class="easyui-textbox"  style="width:400px;"/></td>
					</tr>
					<tr>
						<th>类型</th>
						<td><select id="type" style="width: 80px"></select>
							<div id="sp">
								<input type="radio" name="lang" value="01"><span>1</span><br />
							</div></td>
					</tr>
					<tr>
						<th>从</th>
						<td><input name="beginDate" id="beginDate" type="text"
							class="easyui-datetimebox" /></td>
					</tr>
					<tr>
						<th>终止</th>
						<td><input name="endDate" id="endDate" type="text"
							class="easyui-datetimebox" /></td>
					</tr>
					<tr>
						<th>Billable Lifetime</th>
						<td><input name="countSceond" id="countSceond" type="text"
							class="easyui-textbox" style="width:70px;"/></td>
					</tr>
					<tr>
						<th>Trackable Lifetime</th>
						<td><input name="trackSecond" id="trackSecond" type="text"
							class="easyui-textbox" style="width:70px;"/></td>
					</tr>
					<tr>
						<th>DNIS</th>
						<td><input name="dnis" id="dnis" type="text"
							class="easyui-textbox" style="width:70px;"/></td>
					</tr>
				</table>
			</fieldset>
		</form>
	</div>
</div>

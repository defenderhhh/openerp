<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript">
	$(function() {
		$("#form").form({
			url :"${pageContext.request.contextPath}/UomConvertController/UomConvertSave.html",
			onSubmit : function() {
				parent.$.messager.progress({
					title : '提示',
					text : '数据处理中，请稍后....'
				});
				var isValid = $(this).form('validate');
				if (!isValid) {
					parent.$.messager.progress('close');
				}
				return isValid;
			},
			success : function(result) {
				parent.$.messager.progress('close');
				result = $.parseJSON(result);
				if (result.success) {
					parent.$.modalDialog.openner.datagrid('reload');//之所以能在这里调用到parent.$.modalDialog.openner_datagrid这个对象，是因为user.jsp页面预定义好了
					parent.$.modalDialog.handler.dialog('close');
					parent.$.messager.show({
						title : '提示',
						msg : result.msg,
						timeout : 1000 * 2
					});
				}else{
					parent.$.messager.show({
						title :  '提示',
						msg : result.msg,
						timeout : 1000 * 2
					});
				}
			}
		});
	});
	
</script>
<div class="easyui-layout" data-options="fit:true,border:false">
	<div data-options="region:'center',border:false" title="" style="overflow: hidden;padding: 10px;">
		<form id="form" method="post">
			<fieldset>
				 <table>
					 <tr>
					    <th>持有货币</th>
						<td><select name="uom_id" id="uom_id">
						      <c:forEach items="${uomList }" var="uom">
						          <option value="${uom.id }">${uom.description }</option>
						      </c:forEach>  
						    </select>
						
						<!-- <input name="uom_id" id="uom_id" type="text" class="easyui-textbox easyui-validatebox" required="required"/> --></td>
					 </tr>
                         <tr>
                        <th>兑换货币</th>
                        <td><select name="c_uom_Id" id="c_uom_Id">
                              <c:forEach items="${uomList }" var="uom">
                                  <option value="${uom.id }">${uom.description }</option>
                              </c:forEach>  
                            </select>
                        <!-- <input name="c_uom_Id" id="c_uom_Id" type="text" class="easyui-textbox easyui-validatebox" required="required"/> --></td>
                     </tr>
                          <tr>
                        <th>汇率</th>
                        <td><input name="convert" id="convert" type="text" class="easyui-textbox easyui-validatebox easyui-numberbox" precision="4" missingMessage="必须填写数字" required="required" validType="double"/></td>
                     </tr>
				 </table>
			</fieldset>
		</form>
	</div>
</div>

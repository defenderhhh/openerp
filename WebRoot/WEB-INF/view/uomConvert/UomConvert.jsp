<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>汇率转换管理</title>
    
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">    
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="This is my page">
    <jsp:include page="../common.jsp"></jsp:include>
    <script type="text/javascript">
        var $pt;
        var $grid;
        $(function() {
            $pt = $("#pt");
            $grid = $pt.datagrid({
                url : "${pageContext.request.contextPath}/UomConvertController/list.html",
                width : 'auto',
                height :  $(this).height(),
                pagination:true,
                rownumbers:true,
                border:false,
                singleSelect:true,
                checkOnSelect : false,
                selectOnCheck : false,
                striped:true,
                columns : [ [ {field : 'id',title : '类别编号',width : parseInt($(this).width()*0.1),align : 'left',editor : "text"},
                              {field : 'uom_id',title : '转换',width : parseInt($(this).width()*0.1)},
                              {field : 'c_uom_Id',title : '转换至',width : parseInt($(this).width()*0.1)},
                              {field : 'convert',title : '转换率',width : parseInt($(this).width()*0.1)},
                               {field : 'last_update',title : '最后更新时间',width : parseInt($(this).width()*0.1)},
                                 {field : 'addUser_ID',title : '创建者',width : parseInt($(this).width()*0.1)},
                                 {field : 'create_date',title : '创建时间',width : parseInt($(this).width()*0.1)},
                              ] ],toolbar:'#tb'
            });
        });
        
        //弹窗增加
            function addRow() {
                parent.$.modalDialog({
                    title : "添加汇率转换类型",
                    width : 600,
                    height : 400,
                    href : "${pageContext.request.contextPath}/UomConvertController/UomConvertAdd.html",
                    buttons : [ {
                        text : '保存',
                        iconCls : 'icon-ok',
                        handler : function() {
                            $.modalDialog.openner= $grid;
                            var f = parent.$.modalDialog.handler.find("#form");
                            f.submit();
                        }
                    }, {
                        text : '取消',
                        iconCls : 'icon-cancel',
                        handler : function() {
                            parent.$.modalDialog.handler.dialog('destroy');
                            parent.$.modalDialog.handler = undefined;
                        }
                    }
                    ]
                });
            }
        
        //删除
        function delRow(){
            var row = $pt.datagrid('getSelected');
            if(row){
                var rowIndex = $pt.datagrid('getRowIndex', row);
                parent.$.messager.confirm('询问', '您是否要删除当前记录？', function(b) {
                    if (b) {
                        parent.$.messager.progress({
                            title : '提示',
                            text : '数据处理中，请稍后....'
                        });
                        $.post('${pageContext.request.contextPath}/UomConvertController/UomConvertDel.html', {
                            id : row.id
                        }, function(result) {
                            if (result.success) {
                                parent.$.messager.show({
                                    title : '提示',
                                    msg : result.msg,
                                    timeout : 1000 * 2
                                });
                                $pt.datagrid('deleteRow', rowIndex);
                                parent.$.messager.progress('close');
                            }
                        }, 'JSON');
                    }
                });
            }else{
                parent.$.messager.show({
                    title : "提示",
                    msg : "请选择行数据!",
                    timeout : 1000 * 2
                });
            }
        }
        
        function updateRow() {
            var row = $pt.datagrid('getSelected');
            if(row){
                parent.$.modalDialog({
                    title : "修改汇率转换类型",
                    iconCls : 'icon-ok',
                    width : 600,
                    height : 400,
                    href : "${pageContext.request.contextPath}/UomConvertController/UomConvertEdit.html?id="+row.id,
                    buttons : [ {
                        text : '保存',
                        handler : function() {
                            parent.$.modalDialog.openner= $grid;
                            var f = $.modalDialog.handler.find("#form");
                            f.submit();
                        }
                    }, {
                        text : '取消',
                        iconCls : 'icon-cancel',
                        handler : function() {
                            parent.$.modalDialog.handler.dialog('destroy');
                            parent.$.modalDialog.handler = undefined;
                        }
                    }
                    ]
                });
            }else{
                parent.$.messager.show({
                    title : "提示",
                    msg : "请选择行数据!",
                    timeout : 1000 * 2
                });
            }
            
        }
  </script>
  </head>
  
  <body>
    <div id="tb" style="padding:2px 0">
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td style="padding-left:2px">
                        <a href="javascript:void(0);" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="addRow();">添加</a>
                        <a href="javascript:void(0);" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="delRow();">删除</a>
                        <a href="javascript:void(0);" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="updateRow();">修改</a>
                    </td>
                </tr>
            </table>
        </div>
    <table id="pt" title="汇率转换管理"></table>
  </body>
</html>

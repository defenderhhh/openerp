<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
    <script type="text/javascript">
    	var $roleCombogrid;
    	var $su;
    	var $usergrid;
    	
    	$(function(){
    		$su = $("#su");
    		$roleCombogrid = $('#roleCombogrid').combogrid({
						url : '${pageContext.request.contextPath}/selectUserController/roleList.html',
						panelWidth : 300,
						panelHeight : 200,
						idField : 'id',
						textField : 'name',
						pagination : true,
						fitColumns : true,
						rownumbers : true,
						editable   : false,
						pageSize : 5,
						pageList : [ 5, 10 ],
						columns : [ [ {
							field : 'name',
							title : '角色名称',
							width : 100
						}] ],
						onSelect:function(rowIndex, rowData){
							$usergrid = $su.datagrid({
								url : "${pageContext.request.contextPath}/selectUserController/userList.html?id="+rowData.id,
								width : 'auto',
								//height :  $(this).height(),
								pagination:true,
								rownumbers:true,
								border:false,
								singleSelect:true,
								//checkOnSelect : false,
								//selectOnCheck : false,
								striped:true,
								onCheck:function(rowIndex, rowData){
									$("#userName").val(rowData.userName);
									$("#id").val(rowData.id);
								},
								columns : [ [ {field : 'id',title : '编号',width : parseInt($(this).width()*0.1),align : 'left',checkbox:true},
								              {field : 'userName',title : '用户名',width : parseInt($(this).width()*0.1)},
								              ] ],toolbar:'#usertb'
							});
						}
					});
    		
    		
    		
    		
    		
    		
			
    	});
    
  	</script>
  		
  		<div style="width: auto;height:auto;">
  			<div id="usertb">
				<table cellpadding="0" cellspacing="0">
					<tr>
						<td><input id="roleCombogrid" name="name" type="text" style="height: 29px;" class="easyui-validatebox" ></td>
					</tr>
				</table>
			</div>
	  		<table id="su"></table>
  		</div>
  		
  		
  		

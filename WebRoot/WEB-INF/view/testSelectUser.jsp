<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>选择用户</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<jsp:include page="common.jsp"></jsp:include>
    <script type="text/javascript">
    	var $pt;
    	var $grid;
    	
	  	$(function() {
	  		$pt = $("#pt");
			$grid = $pt.datagrid({
				url : "${pageContext.request.contextPath}/providerTypeController/list.html",
				width : 'auto',
				height :  $(this).height(),
				pagination:true,
				rownumbers:true,
				border:false,
				singleSelect:true,
				checkOnSelect : false,
				selectOnCheck : false,
				striped:true,
				columns : [ [ {field : 'id',title : '类别编号',width : parseInt($(this).width()*0.1),align : 'left',editor : "text"},
				              {field : 'name',title : '类别名称',width : parseInt($(this).width()*0.1)},
				              {field : 'ename',title : '英文名称',width : parseInt($(this).width()*0.1)},
				              {field : 'description',title : '备注',width : parseInt($(this).width()*0.1)},
				              ] ],toolbar:'#tb'
			});
			
			
			
		});
		
		
		function showUser(){
    		parent.$.modalDialog({
					title : "选择用户",
					width : 600,
					height : 400,
					href : "${pageContext.request.contextPath}/selectUserController/index.html",
					buttons : [ {
						text : '确定',
						iconCls : 'icon-ok',
						handler : function() {
							parent.$.modalDialog.handler.dialog('destroy');
							parent.$.modalDialog.handler = undefined;
						}
					}
					]
				});
    	}
  </script>
  <style type="text/css">
  		.button{ background-image: url("${pageContext.request.contextPath}/resource/zoom.png");background-repeat:no-repeat;}
  	</style>
  </head>
  
  <body>
  	<div id="tb" style="padding:2px 0">
			<table cellpadding="0" cellspacing="0">
				<tr>
					<td style="padding-left:2px">
						<span>选择用户</span>
						<input id="userName" name="userName" type="text" style="height: 25px;" >
						<input id="id" name="id" type="hidden" />
						<input type="button" class="button" onclick="showUser();"/>
					</td>
				</tr>
			</table>
		</div>
    <table id="pt" title="供应商类型管理"></table>
  </body>
</html>

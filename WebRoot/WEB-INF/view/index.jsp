<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ taglib uri="/erpTag/DisPlay" prefix="erp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>My JSP 'index.jsp' starting page</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		<meta http-equiv="description" content="This is my page">

	
	
	<jsp:include page="common.jsp"></jsp:include>

	</head>
	<script type="text/javascript">
	function strToObj(str){
		
		str = str.replace(/&/g,"','");
		str = str.replace(/=/g,"':'");
		str = "({'"+str +"'})";
		alert(str);
	    obj = eval(str); 
		return obj;
	}
	function login(){
		var url = $("#loginForm").formSerialize();//转换url
		if(!$("#loginForm").form('validate')){//验证
			
			return;
		}
		$("input[name=type]").each(function(){
			if($(this)[0].checked){
				url = url+"&type="+$(this).val();
			}
		})
		var obj = strToObj(url)
		
		$.ajax({
			  async: true,
			url : '<%=basePath%>j_spring_security_check',
			type : 'post',
			dataType : 'json',
			error : function(XMLResponse) {
			$("#erroMsg").html("错误:"+XMLResponse.responseText)
			},success : function(data) {
				$("#dd").dialog("close");
			}
			,data : obj
		});
		
		
	}
	function init(){
	}
  $(function(){
	  
		$('#cc').combo({
			required:true,
			editable:false
			,width:152
		});
		$('#sp').appendTo($('#cc').combo('panel'));
		$('#sp input').click(function(){
			var v = $(this).val();
			var s = $(this).next('span').text();
			$('#cc').combo('setValue', v).combo('setText', s).combo('hidePanel');
		});
	});
  </script>
	<body class="easyui-layout">
	
		<div id="dd" class="easyui-dialog"
			title="欢迎<erp:display key="company" />"
			style="top: 50%; margin-left50 %; width: 300px; height: 300px;"
			data-options="iconCls:'icon-save',resizable:true,modal:true">
			
	<form id="loginForm">
			<div style="margin-top: 30px;">
				<div style="float: left; margin-left: 20px;">
					<font style="font-family: '微软雅黑';" size="2"><erp:display
							key="user" />:</font>
				</div>
				&nbsp; &nbsp; &nbsp; &nbsp;
				<input name="userName"  class="easyui-validatebox"  data-options="required:true,validType:'length[5,7]'" />
			</div>

			<div style="margin-top: 10px;">
				<div style="float: left; margin-left: 20px;">
					<font style="font-family: '微软雅黑';" size="2"><erp:display
							key="password" />:</font>
				</div>
				&nbsp; &nbsp; &nbsp; &nbsp;
				<input name="passWord"  class="easyui-validatebox"  data-options="required:true,validType:'length[5,7]'" />
			</div>
			<div style="margin-top: 10px;">
				<div style="float: left; margin-left: 20px;">
					<font style="font-family: '微软雅黑';" size="2"><erp:display
							key="type" />:</font>
				</div>
				<div style="float: left; margin-left: 49px;">
					<select class="easyui-validatebox"  data-options="required:true" style="margin-left: 20px;" id="cc"></select>
					<div id="sp">
					
					<c:forEach items="${rolemap}" var="k">
						<div style="color: #99BBE8; background: #fafafa; padding: 5px;">
							
							<input  type="radio" name="type" value="${k.value.id}" />
							<span>${k.value.name}</span>
						</div>
					</c:forEach>
					
						<br />

					</div>
				</div>
				
</form>
			</div>
			<br>
			<br>
			<font  id="erroMsg" style="margin-left: 50px;" color="red"></font>
			<div style="margin-top: 10px; margin-left: 170px;">
				<a href="javascript:login()" class="easyui-linkbutton"
					data-options="iconCls:'icon-ok'"><erp:display key="login" />
				</a>
			</div>
		</div>
		</div>
			<div data-options="region:'north',split:true" title="North Title" style="height:100px;padding:10px;">
		<p>The north content.</p>
	</div>
	<div data-options="region:'south',split:true" title="South Title" style="height:100px;padding:10px;background:#efefef;">
		<div class="easyui-layout" data-options="fit:true" style="background:#ccc;">
			<div data-options="region:'center'">sub center</div>
			<div data-options="region:'east',split:true" style="width:200px;">sub center</div>
		</div>
	</div>
	<div data-options="region:'east',iconCls:'icon-reload',split:true" title="Tree Menu" style="width:180px;">
	</div>
	<!-- 用户管理 -->
	<div data-options="region:'west',split:true" title="<erp:display key="menu" />" style="width:280px;padding1:1px;overflow:hidden;">
		<div class="easyui-accordion" data-options="fit:true,border:false">
			<div title="<erp:display key="manageUser"/>" style="padding:10px;overflow:auto;">
				<p>content1</p>
				<p>content1</p>
				<p>content1</p>
				<p>content1</p>
				<p>content1</p>
				<p>content1</p>
				<p>content1</p>
				<p>content12</p>
			</div>
			<div title="Title2" data-options="selected:true" style="padding:10px;">
				content2
			</div>
			<div title="Title3" style="padding:10px">
				content3
			</div>
		</div>
	</div>
	<div data-options="region:'center'" title="Main Title" style="overflow:hidden;">
		<div class="easyui-tabs" data-options="fit:true,border:false">
			<div title="Tab1" style="padding:20px;overflow:hidden;"> 
				<div style="margin-top:20px;">
					<h3>jQuery EasyUI framework help you build your web page easily.</h3>
					<ul>
						<li>easyui is a collection of user-interface plugin based on jQuery.</li> 
						<li>using easyui you don't write many javascript code, instead you defines user-interface by writing some HTML markup.</li> 
						<li>easyui is very easy but powerful.</li> 
					</ul>
				</div>
			</div>
			<div title="Tab2" data-options="closable:true" style="padding:20px;">This is Tab2 width close button.</div>
			<div title="Tab3" data-options="iconCls:'icon-reload',closable:true" style="overflow:hidden;padding:5px;">
				<table id="tt2"></table> 
			</div>
		</div>
	</div>
	</body>
</html>

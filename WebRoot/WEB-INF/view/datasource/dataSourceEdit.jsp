<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript">
	$(function() {
		$("#form").form({
			url :"${pageContext.request.contextPath}/DataSourceController/dataSourceUpdate.html",
			onSubmit : function() {
				parent.$.messager.progress({
					title : '提示',
					text : '数据处理中，请稍后....'
				});
				var isValid = $(this).form('validate');
				if (!isValid) {
					parent.$.messager.progress('close');
				}
				return isValid;
			},
			success : function(result) {
				parent.$.messager.progress('close');
				result = $.parseJSON(result);
				if (result.success) {
					parent.$.modalDialog.openner.datagrid('reload');
					parent.$.modalDialog.handler.dialog('close');
					parent.$.messager.show({
						title : '提示',
						msg : result.msg,
						timeout : 1000 * 2
					});
				}else{
					parent.$.messager.show({
						title :  '提示',
						msg : result.msg,
						timeout : 1000 * 2
					});
				}
			}
		});
	});
	
</script>
<div class="easyui-layout" data-options="fit:true,border:false">
	<div data-options="region:'center',border:false" title="" style="overflow: hidden;padding: 10px;">
		<form id="form" method="post">
			<fieldset>
				<input name="id" id="id" value="${dataSource.id}"  type="hidden"/>
				 <table>
					 <tr>
                        <th>来源类别id</th>
                        <td><input name="dataSourceTypeId" id="dataSourceTypeId" value="${dataSource.dataSourceTypeId }" type="text" class="easyui-textbox easyui-validatebox" required="required"/></td>
                     </tr>
                         <tr>
                        <th>详情</th>
                        <td><input name="description" id="description" value="${dataSource.description }" type="text" class="easyui-textbox easyui-validatebox"/></td>
                     </tr>
					
				 </table>
			</fieldset>
		</form>
	</div>
</div>

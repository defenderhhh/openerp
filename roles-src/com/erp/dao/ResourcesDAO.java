package com.erp.dao;

import java.util.List;

import com.erp.form.ErpObject;

public interface ResourcesDAO {
	/**
	 * 
	 * @param id
	 *            用户id
	 * @return 用户所有权限得到可以加载的资源
	 */
	public List<ErpObject> findbyuserid(int id);

	/**
	 * 
	 * @return查找全部资源
	 */
	public List<ErpObject> findAll();
}

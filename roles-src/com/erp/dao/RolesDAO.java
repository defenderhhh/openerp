package com.erp.dao;

import java.util.List;

import com.erp.entity.Roles;
import com.erp.form.ErpObject;

public interface RolesDAO{
	/**
	 * 加载所有角色
	 * @return 返回list集合
	 */
	public List<ErpObject> findAll();
	
	public List<Roles> findAllRole();
}

package com.erp.serviceIMP;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.danga.MemCached.MemCachedClient;
import com.erp.Resource.SomeStatic;
import com.erp.dao.RolesDAO;
import com.erp.entity.Roles;
import com.erp.form.ErpObject;
import com.erp.service.RolesServiceDAO;

@Service("rolesService")
@SuppressWarnings({ "unchecked", "rawtypes" })
public class RolesServiceIMP implements RolesServiceDAO,SomeStatic{
	@Autowired
	private RolesDAO rolesMapper;
	@Autowired
	private MemCachedClient memcachedClient;
	
	
	
	
	public MemCachedClient getMemcachedClient() {
		return memcachedClient;
	}

	public void setMemcachedClient(MemCachedClient memcachedClient) {
		this.memcachedClient = memcachedClient;
	}

	public RolesDAO getRolesMapper() {
		return rolesMapper;
	}

	public void setRolesMapper(RolesDAO rolesMapper) {
		this.rolesMapper = rolesMapper;
	}

	/**
	 * 加载所有角色
	 * @return 返回list集合
	 */
	
	public Map<String, Roles> findAll(){
		Object  object = memcachedClient.get(ROLE_MAP);
		Map map = null;
		if(object == null){
			map = new HashMap<String, Roles>();
			List<ErpObject> rolesList = rolesMapper.findAll();
			for (ErpObject roles : rolesList) {//循环添加到map内存
				Roles r = (Roles)roles;
				map.put(String.valueOf(r.getId()), r);
			}
			memcachedClient.set(ROLE_MAP, map);
			return map;
		}else{
			return (HashMap)object;
		}
		
	}
	
	public List<Roles> findAllRole(){
	    
	    return (List<Roles>)rolesMapper.findAllRole();
	}
    
}

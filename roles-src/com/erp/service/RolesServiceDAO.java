package com.erp.service;

import java.util.List;
import java.util.Map;

import com.erp.entity.Roles;


public interface RolesServiceDAO {
	/**
	 * 加载所有角色
	 * @return 返回list集合
	 */
	public Map<String, Roles> findAll();
	
	public List<Roles> findAllRole();
}

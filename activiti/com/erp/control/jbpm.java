package com.erp.control;

import org.activiti.engine.IdentityService;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.User;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.erp.Resource.Resouce;

import antlr.collections.List;


@Controller
@RequestMapping(value="/jbpm")
public class jbpm {
	
	static Logger logger = Logger.getLogger(jbpm.class);
	private String test = "123";
	
	public String getTest() {
		return test;
	}

	public void setTest(String test) {
		this.test = test;
	}

	@Autowired
	private RuntimeService runtimeService;
	@Autowired
	private ProcessEngine processEngine;
	@Autowired
	private TaskService taskService;
	
	
	public TaskService getTaskService() {
		return taskService;
	}

	public void setTaskService(TaskService taskService) {
		this.taskService = taskService;
	}

	public ProcessEngine getProcessEngine() {
		return processEngine;
	}

	public void setProcessEngine(ProcessEngine processEngine) {
		this.processEngine = processEngine;
	}

	public RuntimeService getRuntimeService() {
		return runtimeService;
	}

	public void setRuntimeService(RuntimeService runtimeService) {
		this.runtimeService = runtimeService;
	}

	public jbpm(){
		
	}
//clsss group StartAuthorizationTest
	@RequestMapping(value="index.html")
	public String index(){
		ProcessInstance p = runtimeService.startProcessInstanceByKey("financialReport");

		
		return "/jbpm/index.jsp";
	}
	
	@RequestMapping(value="look.html")
	public String look(){
		
		java.util.List<Task> task = taskService.createTaskQuery().taskCandidateGroup("accountancy").list();//查询该组有那些任务
		for (Task task2 : task) {
			logger.debug("name"+task2.getName());
			logger.debug("getDescription："+task2.getDescription());
			logger.debug("getProcessDefinitionId:"+task2.getProcessDefinitionId());
			taskService.claim(task2.getId(), "user1");
			
		}
		task = taskService.createTaskQuery().taskAssignee("user1").list();//user1 用户有多少任务
		for (Task task2 : task) {
			logger.debug("name"+task2.getName());
			logger.debug("getDescription："+task2.getDescription());
			logger.debug("getProcessDefinitionId:"+task2.getProcessDefinitionId());
		}
		return  "/jbpm/index.jsp";
		
	}
	
	@RequestMapping(value="end.html")
	public String end(){
		
		java.util.List<Task> task = taskService.createTaskQuery().taskAssignee("user1").list();//user1 用户有多少任务
		for (Task task2 : task) {
			logger.debug("name"+task2.getName());
			logger.debug("getDescription："+task2.getDescription());
			logger.debug("getProcessDefinitionId:"+task2.getProcessDefinitionId());
			taskService.complete(task2.getId());
		}
		return  "/jbpm/index.jsp";
	}
	@RequestMapping(value="group.html")
	public String createGroup(){
		IdentityService identityService = processEngine.getIdentityService();
		User user = identityService.newUser("user1");
		Group group = identityService.newGroup("group");
		identityService.createMembership(user.getId(), group.getId());
		return "/jbpm/index.jsp";
		
	}
	
	
}

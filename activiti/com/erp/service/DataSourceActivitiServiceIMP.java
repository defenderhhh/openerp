package com.erp.service;

import org.activiti.engine.RepositoryService;
import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;

import com.erp.jbpm.serviceIMP.DataSourceActivitiService;


public class DataSourceActivitiServiceIMP implements DataSourceActivitiService ,TaskListener {
	RepositoryService repositoryService = null;

	public RepositoryService getRepositoryService() {
		return repositoryService;
	}

	public void setRepositoryService(RepositoryService repositoryService) {
		this.repositoryService = repositoryService;
	}
	
	public void test(){

	}

	public void notify(DelegateTask delegateTask) {
		// TODO Auto-generated method stub
		delegateTask.setAssignee("kermit");
		delegateTask.addCandidateUser("fozzie");
		delegateTask.addCandidateGroup("management");
	}
}

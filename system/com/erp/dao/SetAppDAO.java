package com.erp.dao;

import java.util.List;

import com.erp.entity.SetApp;
import com.erp.form.ErpObject;

/**
 * 功能名: 封装对 tb_set_app表操作<br/>
 * Copyright: Copyright (c) 2012<br/>
 *
 * @author 王迁
 * @version 1.0.0
 */
public interface SetAppDAO {

    /**
     * 功能: 请描述方法的功能
     *
     * @return
     */
    public List<SetApp> findAll();
    
    /**
     * 功能: 请描述方法的功能
     *
     * @param key
     * @return
     */
    public List<SetApp> findByKey(String key);
    
    /**
     * 功能: 请描述方法的功能
     *
     * @param id
     * @return
     */
    public SetApp findById(int id);
    
    /**
     * 功能: 请描述方法的功能
     *
     * @param setApp
     */
    public void add(ErpObject setApp) throws Exception;
    
    /**
     * 功能: 请描述方法的功能
     *
     * @param setApp
     */
    public void update(ErpObject setApp) throws Exception;
    
    /**
     * 功能: 请描述方法的功能
     *
     * @param id
     */
    public void delete(int id) throws Exception;
}

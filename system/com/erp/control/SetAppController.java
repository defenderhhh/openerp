package com.erp.control;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.erp.entity.SetApp;
import com.erp.service.SetAppService;
import com.erp.serviceIMP.SetAppServiceIMP;

@Controller
public class SetAppController {
    
    @Autowired
    SetAppService setAppService;

    @RequestMapping(value="setapp.html")
    public String setApp(HttpServletRequest request,HttpServletResponse resonse,ModelMap map) throws Exception{
        List<SetApp> list = setAppService.findAll();
        map.put("setAppList", list);
        return "setApp/setapp.jsp";
    }
    
    @RequestMapping(value="addsetapp.html")
    public String addsetapp(SetApp setApp) throws Exception{
        setAppService.add(setApp);
        return "redirect:/setapp.html";
    }
    
    @RequestMapping(value="delsetapp.html")
    public String delsetapp(int id,HttpServletRequest request) throws Exception{
        setAppService.delete(id);
        return "redirect:/setapp.html";
    }
    
    @RequestMapping(value="updsetapp.html")
    public String updsetapp(SetApp setApp) throws Exception{
        setAppService.update(setApp);
        return "redirect:/setapp.html";
    }
    
}

package com.erp.service;

import java.util.List;

import com.erp.entity.EmailConfig;
import com.erp.entity.SetApp;


public interface SetAppService {
    /**
     * 功能: 请描述方法的功能
     *
     * @return
     */
    public List<SetApp> findAll() throws Exception ;
    
    /**
     * 功能: 请描述方法的功能
     *
     * @param key
     * @return
     */
    public List<SetApp> findByKey(String key);
    
    /**
     * 功能: 请描述方法的功能
     *
     * @param id
     * @return
     * @throws Exception 
     */
    public SetApp findById(int id) throws Exception  ;
    
    /**
     * 功能: 请描述方法的功能
     *
     * @param setApp
     */
    public void add(SetApp setApp) throws Exception;
    
    /**
     * 功能: 请描述方法的功能
     *
     * @param setApp
     */
    public void update(SetApp setApp)  throws Exception;
    
    /**
     * 功能: 请描述方法的功能
     *
     * @param id
     */
    public void delete(int id)  throws Exception;
    
    public List<String> findSendList();
    
    public EmailConfig getEmailConfig() throws Exception;
}

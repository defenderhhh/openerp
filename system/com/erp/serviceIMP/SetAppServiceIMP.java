package com.erp.serviceIMP;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.danga.MemCached.MemCachedClient;
import com.erp.Resource.SomeStatic;
import com.erp.dao.SetAppDAO;
import com.erp.entity.EmailConfig;
import com.erp.entity.SetApp;
import com.erp.exception.MyException;
import com.erp.service.SetAppService;

/**
 * 功能名: 对SetAppDao的继承和实现<br/>
 * 
 * @author WangQian
 * @version 2013-13-15
 */
@Service("setAppService")
@SuppressWarnings("unchecked")
public class SetAppServiceIMP implements SetAppService, SomeStatic {

    @Autowired
    private MemCachedClient memcachedClient;
    @Autowired
    private SetAppDAO setAppMapper;

    public MemCachedClient getMemcachedClient() {
        return memcachedClient;
    }

    public void setMemcachedClient(MemCachedClient memcachedClient) {
        this.memcachedClient = memcachedClient;
    }

    public SetAppDAO getSetAppMapper() {
        return setAppMapper;
    }

    public void setSetAppMapper(SetAppDAO setApp) {
        this.setAppMapper = setApp;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.erp.dao.SetAppDAO#findAll()
     */

    public List<SetApp> findAll() {
        List<SetApp> list = new ArrayList<SetApp>();
        try {
            SetApp setApp = null;
            for (int i = 0; i < 999; i++) {
                setApp = (SetApp) memcachedClient.get(keyStrategy(i));
                if (setApp != null) {
                    list.add(setApp);
                }
            }
            if (list.size() == 0) {
                list = setAppMapper.findAll();
                for (SetApp sa : list) {
                    memcachedClient.add(keyStrategy(sa.getId()), sa);
                }
            }
        } catch (Exception e) {
            throw new MyException(e.getMessage());
        }
        return list;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.erp.dao.SetAppDAO#findByKey(java.lang.String)
     */
    public List<SetApp> findByKey(String key) {
        List<SetApp> list = findAll();
        List<SetApp> setAppList = new ArrayList<SetApp>();
        try {
            for (SetApp sa : list) {
                if (sa.getKey().equalsIgnoreCase(key)) {
                    setAppList.add(sa);
                }
            }
        } catch (Exception e) {
            throw new MyException(e.getMessage());
        }
        return setAppList;
    }

    public List<String> findSendList() {
        List<String> sendList = new ArrayList<String>();
        List<SetApp> list = findAll();
        try {
            for (SetApp sa : list) {
                if (sa.getKey().equalsIgnoreCase("emailSend")) {
                    sendList.add(sa.getValue());
                }
            }
        } catch (Exception e) {
            throw new MyException(e.getMessage());
        }
        return sendList;
    }

    /**
     * 功能: 获取发送邮件的信息
     * 
     * @return
     */
    public EmailConfig getEmailConfig() throws Exception {
        EmailConfig ec = EmailConfig.getInstance();
        List<SetApp> list = findAll();
        
        
        if (list.size()==0) {
            list.add(setAppMapper.findByKey("auth").get(0));
            list.add(setAppMapper.findByKey("port").get(0));
            list.add(setAppMapper.findByKey("protocol").get(0));
            list.add(setAppMapper.findByKey("host").get(0));
        }
        for (SetApp sa : list) {
            if (sa.getKey().equalsIgnoreCase("auth"))
                ec.setAuth(sa.getValue());
            else if (sa.getKey().equalsIgnoreCase("port"))
                ec.setPort(sa.getValue());
            else if (sa.getKey().equalsIgnoreCase("host"))
                ec.setHost(sa.getValue());
            else if (sa.getKey().equalsIgnoreCase("protocol"))
                ec.setProtocol(sa.getValue());
        }
    
        return ec;
    }
    
    

        //判断逻辑错误，待修改2013、10、27 WangQian
    public SetApp findById(int id) throws Exception {
        List<SetApp> list = (List<SetApp>) memcachedClient.get(SET_APP_KEY);
        if (list == null) {
            list = setAppMapper.findAll();
            memcachedClient.add(SET_APP_KEY, list);
        }
        SetApp setApp = new SetApp();
        for (SetApp sa : list) {
            if (sa.getId() == id) {
                setApp = sa;
                break;
            }
        }
        return setApp;
    }

    public void add(SetApp setApp) throws Exception {
        try {
            setAppMapper.add(setApp);
        } catch (Exception e) {
            throw new MyException(e.getMessage());
        }
        // TODO
        int id = 0;
        setApp.setId(id);
        memcachedClient.add(keyStrategy(id), setApp);
        // syncronizedMemcached();
    }

    public void update(SetApp setApp) throws Exception {
        try {
            setAppMapper.update(setApp);
        } catch (Exception e) {
            throw new MyException(e.getMessage());
        }
        if (memcachedClient.get(keyStrategy(setApp.getId())) != null) {
            memcachedClient.replace(keyStrategy(setApp.getId()), setApp);
        } else {
            memcachedClient.add(keyStrategy(setApp.getId()), setApp);
        }

        // syncronizedMemcached();
    }

    public void delete(int id) throws Exception {
        try {
            setAppMapper.delete(id);
        } catch (Exception e) {
            throw new MyException(e.getMessage());
        }
        memcachedClient.delete(keyStrategy(id));
        // syncronizedMemcached();
    }

    protected String keyStrategy(int id) {
        return "setapp" + id;
    }

    // protected void syncronizedMemcached(){
    // //保持一致
    // List<SetApp> list = setAppMapper.findAll();
    // if(memcachedClient.get(SET_APP_KEY)==null){
    // memcachedClient.add(SET_APP_KEY, list);
    // }else{
    // memcachedClient.replace(SET_APP_KEY, list);
    // }
    // }

}

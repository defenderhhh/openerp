package com.erp.service;

import java.util.List;

import com.erp.entity.ClientType;
import com.erp.exception.MyException;

/**
 * 功能名: 请填写功能名<br/>
 * 功能描述: 请简要描述功能的要点<br/>
 * Copyright: Copyright (c) 2012<br/>
 * 公司: 曙光云计算有限公司<br/>
 *
 * @author Administrator
 * @version 1.0.0
 */
public interface ClientTypeService {

    public List<ClientType> findAll();
    
    public List<ClientType> findByList(int offset, int length);
    
    public ClientType findById(int id);
    
    public void add(ClientType clientType) throws MyException;
    
    public void update(ClientType clientType) throws MyException;
    
    public void delete(int id) throws MyException;
}

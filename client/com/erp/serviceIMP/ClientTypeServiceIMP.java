package com.erp.serviceIMP;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.danga.MemCached.MemCachedClient;
import com.erp.dao.ClientTypeDAO;
import com.erp.entity.ClientType;
import com.erp.exception.MyException;
import com.erp.service.ClientTypeService;

@Service("clientTypeService")
public class ClientTypeServiceIMP implements ClientTypeService{

    @Autowired
    ClientTypeDAO clientTypeMapper;
    @Autowired
    private MemCachedClient memcachedClient;
    
    public List<ClientType> findAll() {
        return clientTypeMapper.findAll();
    }

    public List<ClientType> findByList(int offset, int length) {
        return clientTypeMapper.findByList(offset, length);
    }

    public ClientType findById(int id) {
        return clientTypeMapper.findById(id);
    }

    public void add(ClientType clientType) throws MyException {
        clientTypeMapper.add(clientType);        
    }

    public void update(ClientType clientType) throws MyException {
        clientTypeMapper.update(clientType);
    }

    public void delete(int id) throws MyException {
        clientTypeMapper.delete(id);
    }

}

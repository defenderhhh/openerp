package com.erp.dao;

import java.util.List;

import com.erp.entity.Industry;
import com.erp.form.ErpObject;


/**
 * 对Industry进行数据库层面的增删改查操作
 *
 * @author WangQian
 * @version 1.0.0
 */
public interface IndustryDAO {

    
    /**
     * 功能: 查询所有的Industry
     *
     * @return
     */
    public List<Industry> findAll();
    /**
     * 功能: 按照id查询Industry
     *
     * @param id
     * @return
     */
    public Industry findById(int id);
    /**
     * 功能: 更新记录
     *
     * @param Industry
     * @throws Exception
     */
    public void update(ErpObject industry) throws Exception;
    /**
     * 功能: 增加记录
     *
     * @param Industry
     * @throws Exception
     */
    public int insert(ErpObject industry) throws Exception;
    /**
     * 功能: 删除Industry单条记录
     *
     * @param id
     * @throws Exception
     */
    public void delete(int id) throws Exception;
    
}

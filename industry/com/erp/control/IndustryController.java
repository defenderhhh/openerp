package com.erp.control;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.erp.Resource.Resouce;
import com.erp.entity.Industry;
import com.erp.serviceIMP.IndustryServiceIMP;
import com.erp.util.Json;

@Controller
@RequestMapping(value="/IndustryController")
public class IndustryController {
    
    @Autowired
    IndustryServiceIMP indutryService;
    
    @RequestMapping(value="index.html")
    public String index(){
        return "industry/Industry.jsp";
    }
    
    @ResponseBody
    @RequestMapping(value="list.html")
    public List<Industry> getProviderTypeList(){
        return indutryService.findAll();
    }
    
    @RequestMapping(value="IndustryAdd.html")
    public String IndustryAdd(){
        return "industry/IndustryAdd.jsp";
    }
    
    @RequestMapping(value="IndustryEdit.html")
    public String IndustryEdit(int id,ModelMap map){
        Industry industry = indutryService.findById(id);
        map.put("industry", industry);
        return "industry/IndustryEdit.jsp";
    }
    
    /**
     * 功能: 添加来源管理
     * 
     * @return Json
     */
    @RequestMapping(value="IndustrySave.html")
    @ResponseBody
    public Json save(Industry industry) {
        Json j = new Json();
        try {
            industry.setAddUserId(Resouce.getInstall().getUserId());
            industry.setCreate_time(new Timestamp(System.currentTimeMillis()));
            indutryService.insert(industry);
            j.setSuccess(true);
            j.setMsg("添加成功！");
        } catch (Exception e) {
            j.setMsg(e.getMessage());
        }
        return j;
    }
    
    /**
     * 功能: 删除来源管理
     * 
     * @return Json
     */
    @RequestMapping(value="IndustryDel.html")
    @ResponseBody
    public Json delete(int id){
        Json j = new Json();
        try{
            indutryService.delete(id);
            j.setMsg("删除成功！");
            j.setSuccess(true);
        }catch(Exception e){
            j.setMsg(e.getMessage());
        }
        return j;
    }
    
    
    /**
     * 功能: 修改来源管理
     * 
     * @return Json
     */
    @RequestMapping(value="IndustryUpdate.html")
    @ResponseBody
    public Json update(Industry industry) {
        Json j = new Json();
        try {
            industry.setCreate_time(new Timestamp(System.currentTimeMillis()));
            indutryService.update(industry);
            j.setSuccess(true);
            j.setMsg("修改成功！");
        } catch (Exception e) {
            j.setMsg(e.getMessage());
        }
        return j;
    }
    
  
}

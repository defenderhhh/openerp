package com.erp.dao;

import java.util.List;

import com.erp.form.ErpObject;

/**
 * 
 * @author 尹男男
 *供应商增删改查
 */
public interface ProviderDAO {
	/**
	 * 查询当前工作流所有在审核刚添加的供应商
	 * @return
	 */
	public List<ErpObject> findByFlowStatus();
}

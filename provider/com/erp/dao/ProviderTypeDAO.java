package com.erp.dao;

import java.util.List;

import com.erp.entity.ProviderType;
import com.erp.form.ErpObject;

/**
 * 功能名: 供应商类别接口<br/>
 * 功能描述: 供应商类别增删改查<br/>
 * Copyright: Copyright (c) 2013<br/>
 * 公司: xx有限公司<br/>
 *
 * @author liuhai
 * @version 1.0.0
 */
public interface ProviderTypeDAO {
    /**
     * 功能: 获取所有供应商类别
     *
     * @return List<ProviderType>
     */
    public List<ProviderType> findAll();
    
    /**
     * 功能: 删除供应商类别
     *
     * @return void
     */
    public void deleteById(ErpObject providerType);
    
    /**
     * 功能: 添加供应商类别
     *
     * @return void
     */
    public void save(ErpObject providerType);
    
    /**
     * 功能: 更新供应商类别
     *
     * @return void
     */
    public void update(ErpObject providerType);
    
    /**
     * 功能: 获取供应商类别
     *
     * @return void
     */
    public ErpObject findById(ErpObject providerType);
}

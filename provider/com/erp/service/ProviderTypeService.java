package com.erp.service;

import java.util.List;

import com.erp.entity.ProviderType;

import com.erp.form.ErpObject;
import com.erp.form.ProviderTypeForm;

/**
 * 功能名: 供应商类型sevice接口<br/>
 * 功能描述: 供应商类型sevice接口<br/>
 * Copyright: Copyright (c) 2013<br/>
 * 公司: xx有限公司<br/>
 *
 * @author liuhai
 * @version 1.0.0
 */
public interface ProviderTypeService {
    
    /**
     * 功能: 获取所有供应商类别
     *
     * @return List<ProviderType>
     */
    public List<ProviderType> findAll();
    
    /**
     * 功能: 删除供应商类别
     *
     * @return void
     * @throws Exception 
     */
    public void deleteById(ProviderTypeForm id) throws Exception;
    
    /**
     * 功能: 添加供应商类别
     *
     * @return void
     * @throws Exception 
     */
    public void save(ProviderTypeForm providerTypeForm) throws Exception;
    
    /**
     * 功能: 更新供应商类别
     *
     * @return void
     * @throws Exception 
     */
    public void update(ProviderTypeForm providerTypeForm) throws Exception;
    
    /**
     * 功能: 删除供应商类别
     *
     * @return ErpObject
     */
    public ErpObject findById(ProviderTypeForm id);
}

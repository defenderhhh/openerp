package com.erp.serviceIMP;

import java.util.List;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.erp.dao.ProviderTypeDAO;
import com.erp.entity.ProviderType;

import com.erp.form.ErpObject;
import com.erp.form.ProviderTypeForm;
import com.erp.service.ProviderTypeService;
/**
 * 
 * @author 像风一样自由
 *
 */
/**
 * 功能名: 供应商类型sevice接口实现<br/>
 * 功能描述: 供应商类型sevice接口实现<br/>
 * Copyright: Copyright (c) 2013<br/>
 * 公司: 曙光云计算有限公司<br/>
 *
 * @author liuhai
 * @version 1.0.0
 */
@Service("providerTypeService")
public class ProviderTypeServiceIMP implements ProviderTypeService {

    @Autowired
    private ProviderTypeDAO providerTypeMapper;
    
    /**
     * 功能: 获取所有供应商类别
     *
     * @return List<ProviderType>
     */
    public List<ProviderType> findAll() {
        return (List<ProviderType>)providerTypeMapper.findAll();
    }
    

/**
 * 功能: 删除供应商类别
 *
 * @return void
 */
    public void deleteById(ProviderTypeForm id) throws Exception{

   

        providerTypeMapper.deleteById(id);
    }

    /**
     * 功能: 添加供应商类别
     *
     * @return void
     */
    public void save(ProviderTypeForm providerTypeForm) throws Exception {

 
        ProviderType providerType = new ProviderType();
        BeanUtils.copyProperties(providerTypeForm,providerType,new String[] { "id" });
        providerTypeMapper.save(providerType);
    }
    

    public void update(ProviderTypeForm providerTypeForm) throws Exception{

        ProviderType providerType = new ProviderType();
        int id = providerTypeForm.getId();
        providerType.setId(id);
        BeanUtils.copyProperties(providerTypeForm,providerType,new String[] { "id" });
        providerTypeMapper.update(providerType);
    }

    /**
     * 功能: 删除供应商类别
     *
     * @return ErpObject
     */
    public ErpObject findById(ProviderTypeForm id) {
        return providerTypeMapper.findById(id);
    }
    
    
}

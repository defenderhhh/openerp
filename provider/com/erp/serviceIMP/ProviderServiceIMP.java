package com.erp.serviceIMP;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.erp.dao.ProviderDAO;
import com.erp.service.ProviderService;

@Service("providerService")
public class ProviderServiceIMP implements ProviderService  {
	@Autowired
	private ProviderDAO ProviderMapper;

	public ProviderDAO getProviderMapper() {
		return ProviderMapper;
	}

	public void setProviderMapper(ProviderDAO providerMapper) {
		ProviderMapper = providerMapper;
	}
	
}

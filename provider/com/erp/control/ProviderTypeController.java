package com.erp.control;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.erp.entity.ProviderType;
import com.erp.form.ProviderTypeForm;
import com.erp.service.ProviderTypeService;
import com.erp.util.Json;

/**
 * 功能名: 供应商类型controler<br/>
 * 功能描述: 供应商类型增删改查<br/>
 * Copyright: Copyright (c) 2013<br/>
 * 公司: 曙光云计算有限公司<br/>
 *
 * @author liuhai
 * @version 1.0.0
 */
@Controller
@RequestMapping("/providerTypeController")
public class ProviderTypeController {

    @Autowired
    private ProviderTypeService providerTypeService;
    
    /**
     * 功能: 跳转到供应商类型页面
     *
     * @return String
     */
    @RequestMapping(value="index.html",method = RequestMethod.GET)
    public String providerTypePage(){
        return "provider/providerType.jsp";
    }
    
    /**
     * 功能: 跳转到添加供应商类型页面
     *
     * @return String
     */
    @RequestMapping(value="providerTypeAdd.html")
    public String providerTypeAddPage(){
        return "provider/providerTypeAdd.jsp";
    }
    
    /**
     * 功能: 跳转到修改供应商类型页面
     *
     * @param id
     * @return ModelAndView
     */
    @RequestMapping(value="providerTypeEdit.html")
    public ModelAndView providerTypeEditPage(ProviderTypeForm id){
        ModelAndView mav = new ModelAndView();
        ProviderType providerType = (ProviderType) providerTypeService.findById(id);
        mav.addObject("providerType",providerType);
        mav.setViewName("provider/providerTypeEdit.jsp");
        return mav;
    }
    
    /**
     * 功能: 获取供应商类型列表，自动将List转化为json传到页面
     *
     * @return List<ProviderType>
     */
    @ResponseBody
    @RequestMapping(value="list.html",method = RequestMethod.POST)
    public List<ProviderType> getProviderTypeList(){
        return providerTypeService.findAll();
    }
    
    /**
     * 功能: 添加供应商
     * 
     * @return Json
     */
    @RequestMapping(value="save.html",method = RequestMethod.POST)
    @ResponseBody
    public Json save(ProviderTypeForm providerTypeForm) {
        Json j = new Json();
        try {
            providerTypeService.save(providerTypeForm);
            j.setSuccess(true);
            j.setMsg("添加成功！");
        } catch (Exception e) {
            j.setMsg(e.getMessage());
        }
        return j;
    }
    
    /**
     * 功能: 删除供应商
     * 
     * @return Json
     */
    @RequestMapping(value="delete.html",method = RequestMethod.POST)
    @ResponseBody
    public Json delete(ProviderTypeForm id){
        Json j = new Json();
        try{
            providerTypeService.deleteById((id));
            j.setMsg("删除成功！");
            j.setSuccess(true);
        }catch(Exception e){
            j.setMsg(e.getMessage());
        }
        return j;
    }
    
    
    /**
     * 功能: 修改供应商
     * 
     * @return Json
     */
    @RequestMapping(value="update.html",method = RequestMethod.POST)
    @ResponseBody
    public Json update(ProviderTypeForm providerTypeForm) {
        Json j = new Json();
        try {
            providerTypeService.update(providerTypeForm);
            j.setSuccess(true);
            j.setMsg("修改成功！");
        } catch (Exception e) {
            j.setMsg(e.getMessage());
        }
        return j;
    }
    
}

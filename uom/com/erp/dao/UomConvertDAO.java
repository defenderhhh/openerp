package com.erp.dao;

import java.util.List;

import com.erp.entity.UomConvert;
import com.erp.form.ErpObject;


/**
 * 对UomConvert进行数据库层面的增删改查操作
 *
 * @author WangQian
 * @version 1.0.0
 */
public interface UomConvertDAO {

    
    /**
     * 功能: 查询所有的UomConvert
     *
     * @return
     */
    public List<UomConvert> findAll();
    /**
     * 功能: 按照id查询UomConvert
     *
     * @param id
     * @return
     */
    public UomConvert findById(int id);
    /**
     * 功能: 更新记录
     *
     * @param uomConvert
     * @throws Exception
     */
    public void update(ErpObject uomConvert) throws Exception;
    /**
     * 功能: 增加记录
     *
     * @param uomConvert
     * @throws Exception
     */
    public int insert(ErpObject uomConvert) throws Exception;
    /**
     * 功能: 删除UomConvert单条记录
     *
     * @param id
     * @throws Exception
     */
    public void delete(int id) throws Exception;
    
    public List<UomConvert> findByUAndCUomId(ErpObject uomConvert);
    
}

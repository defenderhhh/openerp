package com.erp.dao;

import java.util.List;

import com.erp.entity.Uom;
import com.erp.form.ErpObject;


/**
 * 对Uom进行数据库层面的增删改查操作
 *
 * @author WangQian
 * @version 1.0.0
 */
public interface UomDAO {

    
    /**
     * 功能: 查询所有的Uom
     *
     * @return
     */
    public List<Uom> findAll();
    /**
     * 功能: 按照id查询Uom
     *
     * @param id
     * @return
     */
    public Uom findById(int id);
    /**
     * 功能: 更新记录
     *
     * @param uom
     * @throws Exception
     */
    public void update(ErpObject uom) throws Exception;
    /**
     * 功能: 增加记录
     *
     * @param uom
     * @throws Exception
     */
    public int insert(ErpObject uom) throws Exception;
    /**
     * 功能: 删除Uom单条记录
     *
     * @param id
     * @throws Exception
     */
    public void delete(int id) throws Exception;
    
}

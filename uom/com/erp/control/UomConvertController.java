package com.erp.control;

import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.erp.Resource.Resouce;
import com.erp.dao.UomConvertDAO;
import com.erp.entity.UomConvert;
import com.erp.serviceIMP.UomConvertServiceIMP;
import com.erp.serviceIMP.UomServiceIMP;
import com.erp.util.Json;

@Controller
@RequestMapping(value="/UomConvertController")
public class UomConvertController {
    
    @Autowired
    UomConvertServiceIMP uomConvertService;
    @Autowired
    UomServiceIMP uomService;
    
    @Autowired
    private UomConvertDAO uomConvertMapper;
    
    @RequestMapping(value="index.html")
    public String index(){
        return "uomConvert/UomConvert.jsp";
    }
    
    
    @ResponseBody
    @RequestMapping(value="list.html")
    public List<UomConvert> getProviderTypeList(){
        return uomConvertService.findAll();
    }
    
    @RequestMapping(value="UomConvertAdd.html")
    public String UomAdd(ModelMap map){
        map.put("uomList",uomService.findAll());
        return "uomConvert/UomConvertAdd.jsp";
    }
    
    @RequestMapping(value="UomConvertEdit.html")
    public String UomEdit(int id,ModelMap map){
        UomConvert uomConvert = uomConvertService.findById(id);
        map.put("uomConvert", uomConvert);
        return "uomConvert/UomConvertEdit.jsp";
    }
    
    /**
     * 功能: 添加汇率
     * 
     * @return Json
     */
    @RequestMapping(value="UomConvertSave.html")
    @ResponseBody
    public Json save(UomConvert uomConvert) {
        Json j = new Json();
        try {
            uomConvert.setAddUser_ID(Resouce.getInstall().getUserId());
            uomConvert.setCreate_date(new Timestamp(System.currentTimeMillis()));
            uomConvert.setLast_update(uomConvert.getCreate_date());
            uomConvertService.insert(uomConvert);
            j.setSuccess(true);
            j.setMsg("添加成功！");
        } catch (Exception e) {
            j.setMsg(e.getMessage());
        }
        return j;
    }
    
    
    /**
     * 功能: 删除汇率
     * 
     * @return Json
     */
    @RequestMapping(value="UomConvertDel.html")
    @ResponseBody
    public Json delete(int id){
        Json j = new Json();
        try{
            uomConvertService.delete(id);
            j.setMsg("删除成功！");
            j.setSuccess(true);
        }catch(Exception e){
            j.setMsg(e.getMessage());
        }
        return j;
    }
    
    
    /**
     * 功能: 修改汇率
     * 
     * @return Json
     */
    @RequestMapping(value="UomConvertUpdate.html")
    @ResponseBody
    public Json update(UomConvert uomConvert) {
        Json j = new Json();
        try {
            uomConvert.setLast_update(new Timestamp(System.currentTimeMillis()));
            uomConvertService.update(uomConvert);
            j.setSuccess(true);
            j.setMsg("修改成功！");
        } catch (Exception e) {
            j.setMsg(e.getMessage());
        }
        return j;
    }
    
    @RequestMapping(value="test.html")
    public void test(){
        UomConvert uc2 = new UomConvert();
        uc2.setC_uom_Id(1);
        uc2.setUom_id(1);
        List<UomConvert> uc = uomConvertMapper.findByUAndCUomId(uc2);
        System.out.println(uc.size()==0);
    }
    
    
  
}

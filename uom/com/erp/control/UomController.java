package com.erp.control;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.erp.Resource.Resouce;
import com.erp.entity.Uom;
import com.erp.serviceIMP.UomServiceIMP;
import com.erp.util.Json;

@Controller
@RequestMapping(value="/UomController")
public class UomController {
    
    @Autowired
    UomServiceIMP uomService;
    
    @RequestMapping(value="index.html")
    public String index(){
        return "uom/Uom.jsp";
    }
    
    @ResponseBody
    @RequestMapping(value="list.html")
    public List<Uom> getProviderTypeList(){
        return uomService.findAll();
    }
    
    @RequestMapping(value="UomAdd.html")
    public String UomAdd(){
        return "uom/UomAdd.jsp";
    }
    
    @RequestMapping(value="UomEdit.html")
    public String UomEdit(int id,ModelMap map){
        Uom uom = uomService.findById(id);
        map.put("uom", uom);
        return "uom/UomEdit.jsp";
    }
    
    /**
     * 功能: 添加汇率
     * 
     * @return Json
     */
    @RequestMapping(value="UomSave.html")
    @ResponseBody
    public Json save(Uom uom) {
        Json j = new Json();
        try {
            uom.setAddUserId(Resouce.getInstall().getUserId());
            uom.setCreate_date(new Timestamp(System.currentTimeMillis()));
            uom.setLast_update_date(uom.getCreate_date());
            uomService.insert(uom);
            j.setSuccess(true);
            j.setMsg("添加成功！");
        } catch (Exception e) {
            j.setMsg(e.getMessage());
        }
        return j;
    }
    
    /**
     * 功能: 删除汇率
     * 
     * @return Json
     */
    @RequestMapping(value="UomDel.html")
    @ResponseBody
    public Json delete(int id){
        Json j = new Json();
        try{
            uomService.delete(id);
            j.setMsg("删除成功！");
            j.setSuccess(true);
        }catch(Exception e){
            j.setMsg(e.getMessage());
        }
        return j;
    }
    
    
    /**
     * 功能: 修改汇率
     * 
     * @return Json
     */
    @RequestMapping(value="UomUpdate.html")
    @ResponseBody
    public Json update(Uom uom) {
        Json j = new Json();
        try {
            uom.setLast_update_date(new Timestamp(System.currentTimeMillis()));
            uomService.update(uom);
            j.setSuccess(true);
            j.setMsg("修改成功！");
        } catch (Exception e) {
            j.setMsg(e.getMessage());
        }
        return j;
    }
    
  
}

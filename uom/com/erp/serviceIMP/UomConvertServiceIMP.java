package com.erp.serviceIMP;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.danga.MemCached.MemCachedClient;
import com.erp.dao.UomConvertDAO;
import com.erp.entity.UomConvert;
import com.erp.exception.MyException;
import com.erp.service.UomConvertService;

@Service(value = "uomConvertService")
public class UomConvertServiceIMP implements UomConvertService {

    @Autowired
    private MemCachedClient memcachedClient;
    @Autowired
    private UomConvertDAO uomConvertMapper;

    private DecimalFormat formater = new DecimalFormat("0.0000");

    public MemCachedClient getMemcachedClient() {
        return memcachedClient;
    }

    public void setMemcachedClient(MemCachedClient memcachedClient) {
        this.memcachedClient = memcachedClient;
    }

    public UomConvertDAO getUomMapper() {
        return uomConvertMapper;
    }

    public void setUomMapper(UomConvertDAO uomMapper) {
        this.uomConvertMapper = uomMapper;
    }

    public List<UomConvert> findAll() {
        List<UomConvert> list = new ArrayList<UomConvert>();
        try {
            UomConvert uomConvert = null;
            for (int i = 0; i < maxRecord; i++) {
                uomConvert = (UomConvert) memcachedClient.get(keyStrategy(i));
                if (uomConvert != null) {
                    list.add(uomConvert);
                }
            }
            if (list.size() == 0) {
                list = uomConvertMapper.findAll();
                for (UomConvert uo : list) {
                    memcachedClient.add(keyStrategy(uo.getId()), uo);
                }
            }
        } catch (Exception e) {
            throw new MyException(e.getMessage());
        }
        return list;
    }

    public UomConvert findById(int id) {
        UomConvert uomConvert = (UomConvert) memcachedClient.get(keyStrategy(id));
        if (uomConvert == null) {
            uomConvert = uomConvertMapper.findById(id);
            if (uomConvert != null) {
                memcachedClient.add(keyStrategy(id), uomConvert);
            }
        }
        return uomConvert;
    }

    /**
     * 
     * 功能: 反向查询
     * 
     * @param uom_id
     * @param c_uom_id
     *            查询与UomConvert记录相对应的另外一条记录
     * @return
     */
    public List<UomConvert> findByUAndCUomId(UomConvert uomConvert) {
        List<UomConvert> list = findAll();
        List<UomConvert> uom = new ArrayList<UomConvert>();
        for (UomConvert uc : list) {
            if (uc.getUom_id() == uomConvert.getC_uom_Id() && uc.getC_uom_Id() == uomConvert.getUom_id()) {
                uom.add(uc);
            }
        }
        if (uom.size()==0) {
            uom = uomConvertMapper.findByUAndCUomId(uomConvert);
            if (uom.size()!=0) {
                for(UomConvert uc:uom){
                memcachedClient.add(keyStrategy(uc.getId()), uc);
                }
            }
        }
        return uom;
    }

    public void update(UomConvert uomConvert) throws Exception {
        UomConvert orginal = findById(uomConvert.getId());
        uomConvertMapper.update(uomConvert);
        memcachedClient.set(keyStrategy(uomConvert.getId()), uomConvertMapper.findById(uomConvert.getId()));
        UomConvert newConvert = new UomConvert(orginal.getC_uom_Id(),orginal.getUom_id());
        List<UomConvert> list =findByUAndCUomId(newConvert);
        if (list.size()!=0) {
            UomConvert queryResult = list.get(0);
            queryResult.setConvert(Double.valueOf(formater.format(1 / uomConvert.getConvert())));
            queryResult.setC_uom_Id(uomConvert.getUom_id());
            queryResult.setUom_id(uomConvert.getC_uom_Id());
            queryResult.setLast_update(uomConvert.getLast_update());
            uomConvertMapper.update(queryResult);
            memcachedClient.set(keyStrategy(queryResult.getId()),queryResult);
        }
        
    }

    public int insert(UomConvert uomConvert) throws Exception {
//        UomConvert uom = findUomReverse(uomConvert);
//        if (uom != null) {
//            throw new Exception("该汇率不可重复添加");
//        }
        uomConvertMapper.insert(uomConvert);
        int id = uomConvert.getId();
        double convert = Double.valueOf(formater.format(1 / uomConvert.getConvert()));
        UomConvert uomConvert2 = new UomConvert(uomConvert.getC_uom_Id(), uomConvert.getUom_id(), convert,
                uomConvert.getCreate_date(), uomConvert.getAddUser_ID(), uomConvert.getCreate_date());
        uomConvertMapper.insert(uomConvert2);
        int id2 = uomConvert2.getId();
        memcachedClient.add(keyStrategy(id), uomConvert);
        memcachedClient.add(keyStrategy(id2), uomConvert2);
        return id;
    }

    public void delete(int id) throws Exception {
        UomConvert uomConvert = uomConvertMapper.findById(id);
        uomConvertMapper.delete(id);
        memcachedClient.delete(keyStrategy(id));
        UomConvert newConvert = new UomConvert(uomConvert.getC_uom_Id(),uomConvert.getUom_id());
        List<UomConvert> list =findByUAndCUomId(newConvert);
        if (list.size()!=0) {
            int newId=list.get(0).getId();
            uomConvertMapper.delete(newId);
            memcachedClient.delete(keyStrategy(newId));
        }
    }

    private static final int maxRecord = 9999;

    private String keyStrategy(int id) {
        return "uomconvert" + id;
    }

}

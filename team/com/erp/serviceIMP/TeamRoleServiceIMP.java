package com.erp.serviceIMP;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.danga.MemCached.MemCachedClient;
import com.erp.dao.TeamRoleDAO;
import com.erp.entity.TeamRole;
import com.erp.service.TeamRoleService;

@Service("teamRoleSerivce")
public class TeamRoleServiceIMP implements TeamRoleService{

    @Autowired
    private MemCachedClient memCachedClient;
    @Autowired
    private TeamRoleDAO teamRoleMapper;
    
    private static final String keyRoleNotNull="team_role_not_null";
    private static final String keyRoleIsNull="team_role_is_null";
    
    @SuppressWarnings("unchecked")
    public List<TeamRole> findAll() {
        List<TeamRole> allList = new ArrayList<TeamRole>();
        List<TeamRole> nullList = (List<TeamRole>) memCachedClient.get(keyRoleIsNull);
        List<TeamRole> notNullList = (List<TeamRole>) memCachedClient.get(keyRoleNotNull);
        allList.addAll(nullList);
        allList.addAll(notNullList);
        return allList;
    }

    public TeamRole findById(int id) {
        List<TeamRole> list = findAll();
        TeamRole role = null;
        for(TeamRole tr:list){
            if(tr.getId()==id){
                role = tr;
            }
        }
        return role;
    }

    public void insert(TeamRole teamRole) throws Exception {
        teamRoleMapper.insert(teamRole);
        synchronizedAll(teamRole);
    }
    

    public void update(TeamRole teamRole) throws Exception {
        teamRoleMapper.update(teamRole);
        synchronizedAll(teamRole);
    }

    public void delete(int id) throws Exception {
        teamRoleMapper.delete(id);
        synchronizedAll();
    }

    @SuppressWarnings("unchecked")
    public List<TeamRole> findNullRole() {
        List<TeamRole> nullList = (List<TeamRole>) memCachedClient.get(keyRoleIsNull);
        if(nullList==null){
            nullList=teamRoleMapper.findNullRole();
            if(nullList!=null){
                memCachedClient.add(keyRoleIsNull, nullList);
            }
        }
        return nullList;
    }

    @SuppressWarnings("unchecked")
    public List<TeamRole> findNotNullRole() {
        List<TeamRole> notNullList = (List<TeamRole>) memCachedClient.get(keyRoleNotNull);
        if(notNullList==null){
            notNullList=teamRoleMapper.findNotNullRole();
            if(notNullList!=null){
                memCachedClient.add(keyRoleNotNull, notNullList);
            }
        }
        return notNullList;
    }
    
    private void synchronizeRoleNotNull(){
        List<TeamRole> list = teamRoleMapper.findNotNullRole();
        memCachedClient.set(keyRoleNotNull, list);
    }
    
    private void synchronizeRoleIsNull(){
        List<TeamRole> list = teamRoleMapper.findNullRole();
        memCachedClient.set(keyRoleIsNull, list);
    }
    
    private void synchronizedAll(){
        synchronizeRoleNotNull();
        synchronizeRoleIsNull();
    }
    
    private void synchronizedAll(TeamRole teamRole){
        if(teamRole.getRole_id()!=0){
        synchronizeRoleNotNull();
        }else{
        synchronizeRoleIsNull();
        }
    }
    
}

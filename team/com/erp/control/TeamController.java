package com.erp.control;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.erp.Resource.Resouce;
import com.erp.entity.Team;
import com.erp.serviceIMP.TeamServiceIMP;
import com.erp.util.Json;

@Controller
@RequestMapping(value="TeamController")
public class TeamController {

    @Autowired
    TeamServiceIMP teamService;
    
    @RequestMapping(value="index.html")
    public String index(){
        return "Team/Team.jsp";
    }
    
    @ResponseBody
    @RequestMapping(value="list.html")
    public List<Team> getProviderTypeList(){
        return teamService.findAll();
    }
    
    @RequestMapping(value="TeamAdd.html")
    public String TeamAdd(){
        return "Team/TeamAdd.jsp";
    }
    
    @RequestMapping(value="TeamEdit.html")
    public String TeamEdit(int id,ModelMap map){
        Team Team = teamService.findById(id);
        map.put("Team", Team);
        return "Team/TeamEdit.jsp";
    }
    
    /**
     * 功能: 添加汇率
     * 
     * @return Json
     */
    @RequestMapping(value="TeamSave.html")
    @ResponseBody
    public Json save(Team team) {
        Json j = new Json();
        try {
            team.setAddUserId(Resouce.getInstall().getUserId());
            teamService.insert(team);
            j.setSuccess(true);
            j.setMsg("添加成功！");
        } catch (Exception e) {
            j.setMsg(e.getMessage());
        }
        return j;
    }
    
    /**
     * 功能: 删除汇率
     * 
     * @return Json
     */
    @RequestMapping(value="TeamDel.html")
    @ResponseBody
    public Json delete(int id){
        Json j = new Json();
        try{
            teamService.delete(id);
            j.setMsg("删除成功！");
            j.setSuccess(true);
        }catch(Exception e){
            j.setMsg(e.getMessage());
        }
        return j;
    }
    
    
    /**
     * 功能: 修改汇率
     * 
     * @return Json
     */
    @RequestMapping(value="TeamUpdate.html")
    @ResponseBody
    public Json update(Team team) {
        Json j = new Json();
        try {
            teamService.update(team);
            j.setSuccess(true);
            j.setMsg("修改成功！");
        } catch (Exception e) {
            j.setMsg(e.getMessage());
        }
        return j;
    }
    
    
}

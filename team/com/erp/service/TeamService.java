package com.erp.service;

import java.util.List;

import com.erp.entity.Team;

public interface TeamService {

    public List<Team> findAll();
    public Team findById(int id);
    public void insert(Team team) throws Exception;
    public void update(Team team) throws Exception;
    public void delete(int id) throws Exception;
    
}

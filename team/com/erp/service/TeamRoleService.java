package com.erp.service;

import java.util.List;

import com.erp.entity.TeamRole;

public interface TeamRoleService {

    public List<TeamRole> findAll();
    public List<TeamRole> findNullRole();
    public List<TeamRole> findNotNullRole();
    public TeamRole findById(int id);
    public void insert(TeamRole teamRole) throws Exception;
    public void update(TeamRole teamRole) throws Exception;
    public void delete(int id) throws Exception;
    
    
}

package com.erp.dao;

import java.util.List;

import com.erp.entity.TeamRole;


public interface TeamRoleDAO {

    public List<TeamRole> findAll();
    public TeamRole findById(int id);
    public void insert(ErpObject teamRole) throws Exception;
    public void update(ErpObject teamRole) throws Exception;
    public void delete(int id) throws Exception;
    public List<TeamRole> findNullRole();
    public List<TeamRole> findNotNullRole();
}

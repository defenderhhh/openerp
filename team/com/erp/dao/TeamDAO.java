package com.erp.dao;

import java.util.List;

import com.erp.entity.Team;

public interface TeamDAO {

    public List<Team> findAll();
    public Team findById(int id);
    public void insert(ErpObject team) throws Exception;
    public void update(ErpObject team) throws Exception;
    public void delete(int id) throws Exception;
    
}

package com.erp.control;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.erp.Resource.Resouce;
import com.erp.entity.DataSource;
import com.erp.serviceIMP.DataSourceServiceIMP;
import com.erp.util.Json;

@Controller
@RequestMapping(value="/DataSourceController")
public class DataSourceController {
    
    @Autowired
    DataSourceServiceIMP dataSourceService;
    
    @RequestMapping(value="index.html")
    public String index(){
        return "datasource/dataSource.jsp";
    }
    
    @ResponseBody
    @RequestMapping(value="list.html")
    public List<DataSource> getProviderTypeList(){
        return dataSourceService.findAll();
    }
    
    @RequestMapping(value="dataSourceAdd.html")
    public String dataSourceAdd(){
        return "datasource/dataSourceAdd.jsp";
    }
    
    @RequestMapping(value="dataSourceEdit.html")
    public String dataSourceEdit(int id,ModelMap map){
        DataSource ds = dataSourceService.findById(id);
        map.put("dataSource", ds);
        return "datasource/dataSourceEdit.jsp";
    }
    
    /**
     * 功能: 添加来源管理
     * 
     * @return Json
     */
    @RequestMapping(value="dataSourceSave.html")
    @ResponseBody
    public Json save(DataSource dataSource) {
        Json j = new Json();
        try {
            dataSource.setAddUserId(Resouce.getInstall().getUserId());
            dataSource.setCreate_date(new Timestamp(System.currentTimeMillis()));
            dataSource.setLastUpdate(dataSource.getCreate_date());
            dataSourceService.insert(dataSource);
            j.setSuccess(true);
            j.setMsg("添加成功！");
        } catch (Exception e) {
            j.setMsg(e.getMessage());
        }
        return j;
    }
    
    /**
     * 功能: 删除来源管理
     * 
     * @return Json
     */
    @RequestMapping(value="dataSourceDel.html")
    @ResponseBody
    public Json delete(int id){
        Json j = new Json();
        try{
            dataSourceService.delete(id);
            j.setMsg("删除成功！");
            j.setSuccess(true);
        }catch(Exception e){
            j.setMsg(e.getMessage());
        }
        return j;
    }
    
    
    /**
     * 功能: 修改来源管理
     * 
     * @return Json
     */
    @RequestMapping(value="dataSourceUpdate.html")
    @ResponseBody
    public Json update(DataSource dataSource) {
        Json j = new Json();
        try {
            dataSource.setLastUpdate(new Timestamp(System.currentTimeMillis()));
            dataSourceService.update(dataSource);
            j.setSuccess(true);
            j.setMsg("修改成功！");
        } catch (Exception e) {
            j.setMsg(e.getMessage());
        }
        return j;
    }
    
  
}

package com.erp.control;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.erp.Resource.Resouce;
import com.erp.entity.DataSourceType;
import com.erp.serviceIMP.DataSourceTypeServiceIMP;
import com.erp.util.Json;

@Controller
@RequestMapping(value="/dataSourceTypeController")
public class DataSourceTypeController {
    
    @Autowired
    DataSourceTypeServiceIMP dataSourceTypeService;
    
    @RequestMapping(value="index.html")
    public String index(){
        return "datasourcetype/dataSourceType.jsp";
    }
    
    @ResponseBody
    @RequestMapping(value="list.html")
    public List<DataSourceType> getProviderTypeList(){
        return dataSourceTypeService.findAll();
    }
    
    @RequestMapping(value="dataSourceTypeAdd.html")
    public String dataSourceAdd(){
        return "datasourcetype/dataSourceTypeAdd.jsp";
    }
    
    @RequestMapping(value="dataSourceTypeEdit.html")
    public String dataSourceEdit(int id,ModelMap map){
        DataSourceType ds = dataSourceTypeService.findById(id);
        map.put("dataSourceType", ds);
        return "datasourcetype/dataSourceEdit.jsp";
    }
    
    /**
     * 功能: 添加来源管理
     * 
     * @return Json
     */
    @RequestMapping(value="dataSourceTypeSave.html")
    @ResponseBody
    public Json save(DataSourceType dataSourceType) {
        Json j = new Json();
        try {
            dataSourceType.setAddUserId(Resouce.getInstall().getUserId());
            dataSourceType.setCreate_data(new Timestamp(System.currentTimeMillis()));
            dataSourceType.setLastUpdateDate(dataSourceType.getCreate_data());
            dataSourceTypeService.insert(dataSourceType);
            j.setSuccess(true);
            j.setMsg("添加成功！");
        } catch (Exception e) {
            e.printStackTrace();
            j.setSuccess(false);
            j.setMsg(e.getMessage());
        }
        return j;
    }
    
    /**
     * 功能: 删除来源管理
     * 
     * @return Json
     */
    @RequestMapping(value="dataSourceTypeDel.html")
    @ResponseBody
    public Json delete(int id){
        Json j = new Json();
        try{
            dataSourceTypeService.delete(id);
            j.setMsg("删除成功！");
            j.setSuccess(true);
        }catch(Exception e){
            j.setMsg(e.getMessage());
        }
        return j;
    }
    
    
    /**
     * 功能: 修改来源管理
     * 
     * @return Json
     */
    @RequestMapping(value="dataSourceTypeUpdate.html")
    @ResponseBody
    public Json update(DataSourceType dataSourceType) {
        Json j = new Json();
        try {
            dataSourceType.setLastUpdateDate(new Timestamp(System.currentTimeMillis()));
            dataSourceTypeService.update(dataSourceType);
            j.setSuccess(true);
            j.setMsg("修改成功！");
        } catch (Exception e) {
            j.setMsg(e.getMessage());
        }
        return j;
    }
    
  
}

package com.erp.dao;

import java.util.List;

import com.erp.entity.DataSource;
import com.erp.form.ErpObject;

/**
 * 对DataSource进行数据库层面的增删改查操作
 *
 * @author WangQian
 * @version 1.0.0
 */
public interface DataSourceDAO {

    
    /**
     * 功能: 查询所有的DataSource
     *
     * @return
     */
    public List<DataSource> findAll();
    /**
     * 功能: 按照id查询DataSource
     *
     * @param id
     * @return
     */
    public DataSource findById(int id);
    /**
     * 功能: 更新记录
     *
     * @param DataSourceActiviticService
     * @throws Exception
     */
    public void update(ErpObject dataSource) throws Exception;
    /**
     * 功能: 增加记录
     *
     * @param DataSourceActiviticService
     * @throws Exception
     */
    public int insert(ErpObject dataSource) throws Exception;
    /**
     * 功能: 删除DataSource单条记录
     *
     * @param id
     * @throws Exception
     */
    public void delete(int id) throws Exception;
    
}

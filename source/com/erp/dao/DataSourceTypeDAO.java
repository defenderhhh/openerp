package com.erp.dao;

import java.util.List;

import com.erp.entity.DataSourceType;
import com.erp.form.ErpObject;

/**
 * 对DataSourceType进行数据库层面的增删改查操作
 *
 * @author WangQian
 * @version 1.0.0
 */
public interface DataSourceTypeDAO {

    
    /**
     * 功能: 查询所有的DataSourceType
     *
     * @return
     */
    public List<DataSourceType> findAll();
    /**
     * 功能: 按照id查询DataSourceType
     *
     * @param id
     * @return
     */
    public DataSourceType findById(int id);
    /**
     * 功能: 更新记录
     *
     * @param dataSourceType
     * @throws Exception
     */
    public void update(ErpObject dataSourceType) throws Exception;
    /**
     * 功能: 增加记录
     *
     * @param dataSourceType
     * @throws Exception
     */
    public int insert(ErpObject dataSourceType) throws Exception;
    /**
     * 功能: 删除DataSourceType单条记录
     *
     * @param id
     * @throws Exception
     */
    public void delete(int id) throws Exception;
    
}

package com.erp.serviceIMP;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.danga.MemCached.MemCachedClient;
import com.erp.dao.DataSourceTypeDAO;
import com.erp.entity.DataSourceType;
import com.erp.exception.MyException;
import com.erp.service.DataSourceTypeService;
@Service("dataSourceTypeService")
public class DataSourceTypeServiceIMP implements DataSourceTypeService {
    
    @Autowired
    private MemCachedClient memcachedClient;
    @Autowired
    private DataSourceTypeDAO dataSourceTypeMapper;

    private static final int maxRecord=9999;
    
    public MemCachedClient getMemcachedClient() {
        return memcachedClient;
    }

    public void setMemcachedClient(MemCachedClient memcachedClient) {
        this.memcachedClient = memcachedClient;
    }

    public DataSourceTypeDAO getDataSourceTypeMapper() {
        return dataSourceTypeMapper;
    }

    public void setDataSourceTypeMapper(DataSourceTypeDAO dataSourceTypeMapper) {
        this.dataSourceTypeMapper = dataSourceTypeMapper;
    }

    public List<DataSourceType> findAll() {
        List<DataSourceType> list = new ArrayList<DataSourceType>();
        try {
            DataSourceType DataSourceType = null;
            for (int i = 0; i < maxRecord; i++) {
                DataSourceType = (DataSourceType) memcachedClient.get(keyStrategy(i));
                if (DataSourceType != null) {
                    list.add(DataSourceType);
                }
            }
            if (list.size() == 0) {
                list = dataSourceTypeMapper.findAll();
                for (DataSourceType ds : list) {
                    memcachedClient.add(keyStrategy(ds.getId()), ds);
                }
            }
        } catch (Exception e) {
            throw new MyException(e.getMessage());
        }
        return list;
    }

    public DataSourceType findById(int id) {
        DataSourceType ds = (DataSourceType) memcachedClient.get(keyStrategy(id));
        if(ds==null){
            ds = dataSourceTypeMapper.findById(id);
            if(ds!=null){
                memcachedClient.add(keyStrategy(id), ds);
            }
        }
        return ds;
    }

    public void update(DataSourceType dataSourceType) throws Exception {
        dataSourceTypeMapper.update(dataSourceType);
        memcachedClient.set(keyStrategy(dataSourceType.getId()), dataSourceType);
    }

    public int insert(DataSourceType dataSourceType) throws Exception {
        dataSourceTypeMapper.insert(dataSourceType);
        int id = dataSourceType.getId();
        memcachedClient.set(keyStrategy(id), dataSourceType);
        return id;
    }

    public void delete(int id) throws Exception {
       dataSourceTypeMapper.delete(id);
       memcachedClient.delete(keyStrategy(id));
    }
    
    private String keyStrategy(int id){
        return "DataSourceType" + id;
    }

}

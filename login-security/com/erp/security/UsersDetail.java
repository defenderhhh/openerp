package com.erp.security;



import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.springframework.security.core.CredentialsContainer;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.Assert;


//不可变类
@SuppressWarnings("serial")
public final class UsersDetail implements UserDetails, CredentialsContainer {

	private static long serialVersionUID = 5997839307263494359L;
	private boolean  enabled = true;
	private String current = null;
	
	public String getCurrent() {
		return current;
	}


	public void setCurrent(String current) {
		this.current = current;
	}



	public UsersDetail() {
		super();
	}



	// ~ Instance fields
	// ================================================================================================
	private String password;
	private int id;
	private String name;// 用户的中文姓名
	private String avatarurl = "";// 头像

	private String emailuser;// 用户的邮箱地址

	private String userName;

	private Set<GrantedAuthority> authorities;

	private boolean accountNonExpired  = true;

	private boolean accountNonLocked = true;

	private boolean credentialsNonExpired = true;

	private String logintime = "2013/20/15 11:31";
	private String requestIp = "192.168.1.1";
	private String sessionid = "test sessionid 12345679";
	
	public String getRequestIp() {
		return requestIp;
	}


	public void setRequestIp(String requestIp) {
		this.requestIp = requestIp;
	}




	public String getLogintime() {
		return logintime;
	}


	public void setLogintime(String logintime) {
		this.logintime = logintime;
	}


	public String getSessionid() {
		return sessionid;
	}


	public void setSessionid(String sessionid) {
		this.sessionid = sessionid;
	}


	public static long getSerialVersionUID() {
		return serialVersionUID;
	}


	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}


	public static void setSerialVersionUID(long serialVersionUID) {
		UsersDetail.serialVersionUID = serialVersionUID;
	}



	public String getPassword() {
		return password;
	}



	public void setPassword(String password) {
		this.password = password;
	}



	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public String getAvatarurl() {
		return avatarurl;
	}



	public void setAvatarurl(String avatarurl) {
		this.avatarurl = avatarurl;
	}



	public String getEmailuser() {
		return emailuser;
	}



	public void setEmailuser(String emailuser) {
		this.emailuser = emailuser;
	}



	public String getUserName() {
		return userName;
	}



	public void setUserName(String userName) {
		this.userName = userName;
	}



	public Set<GrantedAuthority> getAuthorities() {
		return authorities;
	}



	public void setAuthorities(Set<GrantedAuthority> authorities) {
		this.authorities = authorities;
	}



	public boolean isAccountNonExpired() {
		return accountNonExpired;
	}



	public void setAccountNonExpired(boolean accountNonExpired) {
		this.accountNonExpired = accountNonExpired;
	}



	public boolean isAccountNonLocked() {
		return accountNonLocked;
	}



	public void setAccountNonLocked(boolean accountNonLocked) {
		this.accountNonLocked = accountNonLocked;
	}



	public boolean isCredentialsNonExpired() {
		return credentialsNonExpired;
	}



	public void setCredentialsNonExpired(boolean credentialsNonExpired) {
		this.credentialsNonExpired = credentialsNonExpired;
	}



	// ~ Constructors
	// ===================================================================================================
	public UsersDetail(String password, String name,
			String userName,
			Collection<? extends GrantedAuthority> authorities,
		int id) {
	
		this.id = id;
		if (userName == null || "".equals(userName) || password == null)
			throw new IllegalArgumentException("Cannot pass null or empty values to constructor");
		this.password = password;
		this.name = name;
		this.userName = userName;
		this.authorities = Collections
				.unmodifiableSet(sortAuthorities(authorities));
	}
		
	

	private static SortedSet<GrantedAuthority> sortAuthorities(Collection<? extends GrantedAuthority> authorities) {
		Assert.notNull(authorities,"Cannot pass a null GrantedAuthority collection");
		// Ensure array iteration order is predictable (as per
		// UserDetails.getAuthorities() contract and SEC-717)
		SortedSet<GrantedAuthority> sortedAuthorities = new TreeSet<GrantedAuthority>(
				new AuthorityComparator());

		for (GrantedAuthority grantedAuthority : authorities) {
			Assert.notNull(grantedAuthority,"GrantedAuthority list cannot contain any null elements");
			sortedAuthorities.add(grantedAuthority);
		}
		return sortedAuthorities;
	}

	
	private static class AuthorityComparator implements
			Comparator<GrantedAuthority>, Serializable {
		public int compare(GrantedAuthority g1, GrantedAuthority g2) {
			// Neither should ever be null as each entry is checked before
			// adding it to the set.
			// If the authority is null, it is a custom authority and should
			// precede others.
			if (g2.getAuthority() == null) {
				return -1;
			}
			if (g1.getAuthority() == null) {
				return 1;
			}
			return g1.getAuthority().compareTo(g2.getAuthority());
		}
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(super.toString()).append(": ");
		sb.append("Username: ").append(this.userName).append("; ");
		sb.append("Password: [PROTECTED]; ");
		sb.append("Enabled: ").append(this.enabled).append("; ");
		sb.append("AccountNonExpired: ").append(this.accountNonExpired)
				.append("; ");
		sb.append("credentialsNonExpired: ").append(this.credentialsNonExpired)
				.append("; ");
		sb.append("AccountNonLocked: ").append(this.accountNonLocked)
				.append("; ");

		if (!authorities.isEmpty()) {
			sb.append("Granted Authorities: ");

			boolean first = true;
			for (GrantedAuthority auth : authorities) {
				if (!first) {
					sb.append(",");
				}
				first = false;

				sb.append(auth);
			}
		} else {
			sb.append("Not granted any authorities");
		}

		return sb.toString();
	}

	public boolean equals(Object object) {
		if (object instanceof UsersDetail) {
			if (String.valueOf(this.id).equals(String.valueOf(((UsersDetail) object).getId())))
				return true;
		}
		return false;
	}

	public int hashCode() {
		return String.valueOf(this.id).hashCode();
	}



	public void eraseCredentials() {
		// TODO Auto-generated method stub
		
	}



	public String getUsername() {
		// TODO Auto-generated method stub
		return null;
	}



	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return enabled;
	}


}
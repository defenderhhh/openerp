package com.erp.control;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.erp.Resource.SomeStatic;
import com.erp.service.RolesServiceDAO;

/**
 * 
 * @author 尹男男
 * 登录操作加载
 *
 */
@Controller
public class LoginInit implements SomeStatic{
	@Autowired
	private RolesServiceDAO rolesService;

	public RolesServiceDAO getRolesService() {
		return rolesService;
	}

	public void setRolesService(RolesServiceDAO rolesService) {
		this.rolesService = rolesService;
	}
	/**
	 * 登录初始化
	 * @param map
	 * @param request spring 封装
	 * @return 返回的view
	 */
	@RequestMapping(value="login.html",params = "method=init")
	public String addUserInit(ModelMap map, HttpServletRequest request) {
		map.put(ROLE_MAP, rolesService.findAll());//加载所有角色
		return "index.jsp";//返回主页
	}
	/**
	 * 
	 * @param nickname 检查姓名
	 * @return  抛出异常
	 * @throws Exception 自定义异常
	 */
	@RequestMapping(value="login.html",params = "method=out")
	public 
	String checkNickName(ModelMap map,String nickname){
		map.put(ROLE_MAP, rolesService.findAll());//加载所有角色
		//throw new Exception("登录超时");//测试
		return "index.jsp";//返回主页
	}
	/**
	 * 
	 * @return 登录成功返回结果
	 */
	@ResponseBody
	@RequestMapping(value="login.html",params = "method=success")
	public String loginSuccess(){
		
		return "true";
	}

	/**
	 *  spring处理异常操作
	 * @param e
	 * @return
	 */
	@ExceptionHandler(Exception.class)
	public String exception(Exception e) {
		System.out.println(e.getMessage());
		e.printStackTrace();//处理异常
		return e.getMessage();
	}
	
	
	
	
	
}

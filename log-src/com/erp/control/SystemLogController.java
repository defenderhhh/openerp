package com.erp.control;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.erp.service.SystemLogService ;
import com.erp.entity.ErpLog;
import com.erp.entity.SystemLog;
import com.erp.form.ErpObject;
import com.erp.form.SystemLogForm;

/**
 * 功能名: 系统日志controler<br/>
 * 功能描述: 日志的查询操作<br/>
 * @author 李魏
 * @version 1.0.0
 */
@Controller
@RequestMapping("/systemLogController")
public class SystemLogController {

    @Autowired
    private SystemLogService systemLogService;
    /**
     * 功能: 跳转到日志页面
     * @return String
     */
    @RequestMapping(value="index.html",method = RequestMethod.GET)
    public String systemLogPage(){
        return "systemlog/systemlog.jsp";
    }
    
    /**
     * 功能: 获取日志列表，自动将List转化为json传到页面
     * @return List<SystemLog>
     */
    @ResponseBody
    @RequestMapping(value="list.html",method = RequestMethod.POST)
    public List<ErpObject> getSystemLogList(){
        return systemLogService.findAll();
    }
    
    /**
     * 功能: 删除全部日志
     * @return String
     */
    @RequestMapping(value="delAll.html",method = RequestMethod.POST)
    @ResponseBody
    public String delAll(SystemLog ss){
    	systemLogService.delAll();
    	return "systemlog/systemlog.jsp";
    }
    
    /**
     * 功能: 查询功能
     */
    @ErpLog(description="查询日志")
    @RequestMapping(value="query.html",method = RequestMethod.POST)
    @ResponseBody
    public List<ErpObject> query(SystemLogForm systemLogForm){
    	return systemLogService.query(systemLogForm);
    }
}

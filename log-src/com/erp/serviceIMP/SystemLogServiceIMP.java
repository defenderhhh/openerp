package com.erp.serviceIMP;

import java.util.List;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.erp.dao.SystemLogDAO;
import com.erp.entity.SystemLog;
import com.erp.form.ErpObject;
import com.erp.form.SystemLogForm;
import com.erp.service.SystemLogService;
/**
 * 日志系统的接口实现
 * @author 李魏
 *
 */
@Service("systemLogService")
public class SystemLogServiceIMP implements SystemLogService{
	@Autowired
    private SystemLogDAO systemLogMapper;
	public SystemLogDAO getSystemLogMapper() {
		return systemLogMapper;
	}

	public void setSystemLogMapper(SystemLogDAO systemLogMapper) {
		this.systemLogMapper = systemLogMapper;
	}

	public List<ErpObject> findAll() {
		return systemLogMapper.findAll();
	}

	public ErpObject findById(SystemLog id) {
		return systemLogMapper.findById(id);
	}

	public List<ErpObject> query(SystemLogForm systemLogForm) {
		return systemLogMapper.query(systemLogForm);
	}

	public void save(SystemLog systemLogForm) {
		SystemLog systemLog = new SystemLog();
        BeanUtils.copyProperties(systemLogForm,systemLog,new String[] { "id" });
        systemLogMapper.save(systemLog);
	}

	public int delAll() {
		return systemLogMapper.delAll();
	}
	
}

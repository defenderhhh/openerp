package com.erp.dao;

import java.util.List;

import com.erp.entity.SystemLog;
import com.erp.form.ErpObject;
import com.erp.form.SystemLogForm;
/**
 * 
 * @author 李魏
 *
 */
public interface SystemLogDAO {
	/**
	 * 获取所有的日志（注：此方法得分页）
	 * @return
	 */
	public List<ErpObject> findAll();
	    
	/**
	 * 保存日志记录
	 * @param SystemLog
	 */
    public void save(SystemLog SystemLog);
    
    /**
     * 通过日志PK获取日志信息
     * @param id
     * @return
     */
    public ErpObject findById(SystemLog id);
	
    /**
     * 通过进入方法的时间和登陆者查询
     */
    public List<ErpObject> query(SystemLogForm systemLogForm);
    
    /**
     * 删除所有日志
     */
    public int delAll();
}

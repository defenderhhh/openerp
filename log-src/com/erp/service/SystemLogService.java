package com.erp.service;

import java.util.List;
import com.erp.entity.SystemLog;
import com.erp.form.ErpObject;
import com.erp.form.SystemLogForm;

/**
 * 日志类service接口
 * @author 李魏
 *
 */
public interface SystemLogService {
    public List<ErpObject> findAll();
    public void save(SystemLog SystemLog);
    public ErpObject findById(SystemLog id);
    public List<ErpObject> query(SystemLogForm systemLogForm);
    public int delAll();
    
}

package com.erp.exception;

import java.util.List;

import javax.servlet.ServletContext;

import org.springframework.context.ApplicationContext;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.erp.Resource.EmailSender;
import com.erp.Resource.Resouce;
import com.erp.entity.EmailConfig;
import com.erp.service.SetAppService;
import com.erp.serviceIMP.SetAppServiceIMP;

/**
 * 功能名: 异常的处理与发送<br/>
 * 
 * @author WangQian
 * @version 1.0.0
 */
public class MyException extends AuthenticationException {

    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;

    public MyException(String meg) {
        super(meg);
    }

//     @Override
    public String getMessage1(ServletContext sc,String errorMsg) {
        //获取ApplicationContext
        ApplicationContext ac = WebApplicationContextUtils.getWebApplicationContext(sc);
        SetAppService setAppService = (SetAppService) ac.getBean("setAppService");
        List<String> sendList = setAppService.findSendList();
        
        try {
        	  EmailConfig emailConfig = setAppService.getEmailConfig();
              // 然后进行发送操作 把super.getMessage(); 发送过去
              EmailSender.send(sendList.toArray(new String[0]), Resouce.getInstall().nowDate() + "ErrorLog", errorMsg,
                      "text/html",emailConfig);
		} catch (Exception e) {
			// TODO: handle exception
		}
      
        return super.getMessage();
    }

}

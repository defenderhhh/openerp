package com.erp.filter;

import java.util.regex.Pattern;

public class UrlFilter {
	private String url;
	private String type;
	private String request;
	private String gz;
	
	public String getGz() {
		return gz;
	}

	public void setGz(String gz) {
		this.gz = gz;
	}

	public UrlFilter() {
	}

	public UrlFilter(String url) {
		this.url = url;
	}

	public UrlFilter(String url, String type) {
		super();
		this.url = url;
		this.type = type;
	}

	public UrlFilter(String url, String type, String request) {
		super();
		this.url = url;
		this.type = type;
		this.request = request;
	}

	public UrlFilter(String url, String type, String request, String gz) {
		super();
		this.url = url;
		this.type = type;
		this.request = request;
		this.gz = gz;
	}

	public String getUrl() {
		return url;
	}



	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		if
		(request == null){
			this.request = "";
		}else
		this.request = request;
	}

	public void setUrl(String url) {
	
		this.url = url;
	}

	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	@Override
	public int hashCode() {
		
		return 1;
	}
	@Override
	public boolean equals(Object obj) {
		
		Pattern pattern = Pattern.compile(((UrlFilter)obj).url);
		
		return pattern.matcher(this.getUrl()).matches();
	}
}

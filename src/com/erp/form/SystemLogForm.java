package com.erp.form;

import com.erp.entity.SystemLog;
/**
 * SystemLog的form类
 * @author 李魏
 *
 */
public class SystemLogForm extends SystemLog{
	private static final long serialVersionUID = 1L;
	private String start_time ;
	private String end_time ;
	public String getStart_time() {
		return start_time;
	}
	public void setStart_time(String startTime) {
		start_time = startTime;
	}
	public String getEnd_time() {
		return end_time;
	}
	public void setEnd_time(String endTime) {
		end_time = endTime;
	}
	
}

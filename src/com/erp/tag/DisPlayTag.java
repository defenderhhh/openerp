package com.erp.tag;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import com.erp.config.Config;
@SuppressWarnings({ "unused" })
public class DisPlayTag extends TagSupport{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String key;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}
	@Override
	public int doStartTag() throws JspException {
		Config config = new Config(pageContext.getServletContext().getRealPath("/WEB-INF/")+"/cfg/zh/chinese.properties");
		HttpServletRequest request = (HttpServletRequest)pageContext.getRequest();
		JspWriter out = pageContext.getOut();   
		String value = config.getValue(key);
		try {
			out.print(value);
	
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SKIP_BODY;
	}
	@Override
	public int doEndTag() throws JspException {
		// TODO Auto-generated method stub
		return super.doEndTag();
	}
}

package com.erp.listener;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;

import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.erp.Resource.Resouce;
import com.erp.entity.Roles;
import com.erp.filter.UrlFilter;

@SuppressWarnings({ "rawtypes", "unused" })
public class UrlListener  implements   javax.servlet.ServletContextListener{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		System.out.println(1);
	}

	
	public void contextInitialized(ServletContextEvent arg0) {
		if(Resouce.urlMap == null){
			Resouce.urlMap = new HashMap<UrlFilter, UrlFilter>();
			SAXReader sr = new SAXReader();
			org.dom4j.Document doc = null;
			try {
				doc = sr.read(new File(arg0.getServletContext().getRealPath("\\WEB-INF\\")+"/urlrewrite.xml"));
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				System.out.println(e.getMessage());
				Resouce.urlMap = null;
				return;
			}
			String parm = "";
			Element el_root = doc.getRootElement();
			Iterator it = el_root.elementIterator();
			while (it.hasNext()) {
			
				Element type = (Element) it.next();
				parm = type.attributeValue("parm");
				Resouce.urlMap.put(new UrlFilter(type.attributeValue("http"), type.attributeValue("type")),new UrlFilter(type.getText(), type.attributeValue("type"),type.attributeValue("request"),type.attributeValue("http")));
			}
		}
		
		

		
	}

}

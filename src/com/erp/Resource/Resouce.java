package com.erp.Resource;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.security.authentication.encoding.MessageDigestPasswordEncoder;
import org.springframework.web.context.support.WebApplicationContextUtils;
import com.erp.exception.MyException;
import com.erp.filter.UrlFilter;
import com.erp.form.ErpObject;
import com.erp.security.UsersDetail;

@SuppressWarnings({"rawtypes","static-access"})
public class Resouce implements SomeStatic {
	private int phone = 11;// 判断手机用户长度
	static Logger logger = Logger.getLogger(Resouce.class.getName());
	public static HashMap<UrlFilter, UrlFilter> urlMap = null;
	private static Resouce r;
	private boolean oneflash;
	static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd H:m:s");
	public MyException e = new MyException("");
	public  String nowDate() {
		return sdf.format(new Date());
	}

	static {
		try {
			r = (Resouce) Class.forName(RESOURCE_CLASS).newInstance();
			logger.debug("resource  资源类初始化成功:" + r);
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("init error in Resource");
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public ErpObject doPage(ErpObject erp ){
		if(erp.getRows() == 0){
			erp.setRows(10);
		}
		if(erp.getPage() == 0 ||erp.getPage()==1){
			erp.setStart(0);
		}else{
			erp.setStart((erp.getPage()-1)*erp.getRows());
		}
		return erp;
		
	}
	public static Resouce getInstall() {

		return r;
	}

	private Resouce() {

	}

	public int getPhone() {
		return phone;
	}

	public void setPhone(int phone) {
		this.phone = phone;
	}



	/**
	 * 查询所有问题加载到内存
	 * 
	 * @return
	 */

	public static HashMap<UrlFilter, UrlFilter> getUrlMap() {
		return urlMap;
	}

	public static void setUrlMap(HashMap<UrlFilter, UrlFilter> urlMap) {
		Resouce.urlMap = urlMap;
	}

	/**
	 * 密码加密方法
	 * 
	 * @param password
	 * @param username
	 * @return
	 */
	public static String passwordFlite(String password, String username) {
		MessageDigestPasswordEncoder base = new MessageDigestPasswordEncoder(
				"SHA-1", false);
		return base.encodePassword(password, username);
	}

	/**
	 * 
	 * @param index
	 *            根据index位数随机不同随机数
	 * @return
	 */
	public static String rand(int index) {
		Random random = new Random();
		String sRand = "";
		for (int i = 0; i < index; i++) {
			String rand = String.valueOf(random.nextInt(10));
			sRand += rand;

		}
		return sRand;
	}

	/**
	 * 根据传入的name进行获取帐号开头名字
	 * 
	 * @param src
	 * @return
	 */
	public static String getName(String src) {
		char a;
		String result = "";
		int index = 0;
		;
		Pattern pattern = Pattern.compile("[a-zA-Z]+");
		Pattern patternsz = Pattern.compile("[[0-9]]+");
		Pattern zw = Pattern.compile("[\u4E00-\u9FA5]");
		ChineseSpelling finder = ChineseSpelling.getInstance();
		for (int i = 0; i < src.length(); i++) {
			a = src.charAt(i);
			if (patternsz.matcher(String.valueOf(a)).matches()) {
				result = result + a;
				index++;
			} else if (pattern.matcher(String.valueOf(a)).matches()) {
				result = result + a;
				index++;
			} else if (zw.matcher(String.valueOf(a)).matches()) {
				try {
					result = result
							+ finder.convert(String.valueOf(a)).substring(0, 1);
					index++;
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					index--;
					continue;
				}

			} else {
				continue;
			}
			if (index == 3) {
				break;
			}
		}
		return result;
	}

	/*
	 * 检查email 合法性
	 */
	public static boolean checkMail(String email) {
		Pattern pattern = Pattern
				.compile("^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$");
		Matcher matcher = pattern.matcher(email);
		return matcher.matches();

	}

	public static String backJson(String msg) {
		msg = "{\"msg\":\"" + msg + "\"}";

		return msg;
	}

	public static void clearParms(HttpServletRequest request) {
		Enumeration t = request.getAttributeNames();
		Object object = null;
		while (t.hasMoreElements()) {
			object = (Object) t.nextElement();
			if (object != null) {
				if (!object
						.toString()
						.equals("org.springframework.web.context.request.RequestContextListener.REQUEST_ATTRIBUTES")
						&& !object.toString().equals(
								"SetCharacterEncoding.FILTERED")) {
					request.removeAttribute(object.toString());
				}
			}
		}
	}

	public static void main(String[] args) {
		System.out.println(Resouce.passwordFlite("1", "1"));
	}

	/**
	 * 获取用户的工具类
	 * 
	 * @return
	 */
	public UsersDetail getUser() {
		// if
		// (SecurityContextHolder.getContext().getAuthentication().getPrincipal().equals("anonymousUser"))
		// {
		// return null;
		// }
		// UsersDetail user = (UsersDetail)
		// SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		return new UsersDetail();

	}

	/**
	 * 获取用户的id 工具类
	 * 
	 * @return
	 */
	public int getUserId() {
		return 1;
	}
	
	
	
	public void closeFlow(ServletContext sc,int type){
		 ApplicationContext ac = WebApplicationContextUtils.getWebApplicationContext(sc);
		 if(type == 1 ){//供应商添加流程
			 
			 
		 }else if(type == 2 ){//订单审核流程
			 
		 }else if(type == 3 ){
			 
			 
		 }
		 
	}
    public boolean isOneflash() {
        return oneflash;
    }
    public void setOneflash(boolean oneflash) {
        this.oneflash = oneflash;
    }
	
	
}

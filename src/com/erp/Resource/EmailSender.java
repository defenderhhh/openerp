package com.erp.Resource;




import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;

import com.erp.entity.EmailConfig;

public class EmailSender {
    
    private static final String charset = "UTF-8";
    private static final String defaultMimetype = "text/plain";
    
    public static void main(String[] args) throws Exception {
    	String regEx = "a";
    	String str = "a a c d e 1 2 3 4.5";
    	Pattern pattern = Pattern.compile(regEx);
    	Matcher m = pattern.matcher(str);
    	while(m.find(2)) {
    		System.out.println(m.groupCount());
    	for(int i = 0; i<=m.groupCount() ; i++) {
    	System.out.println( i + ": " + m.group(i));
    	}
    	}
    }// Group i substring  }
    /**
     * 发送邮件
     * @param receiver 收件人
     * @param subject 标题
     * @param mailContent 邮件内容
     * @param mimetype 内容类型 默认为text/plain,如果要发送HTML内容,应设置为text/html
     */
    public static void send(String receiver, String subject, String mailContent, String mimetype,EmailConfig emailConfig) {
    	send(new String[]{receiver}, subject, mailContent, mimetype,emailConfig);
    }
    /**
     * 发送邮件
     * @param receivers 收件人
     * @param subject 标题
     * @param mailContent 邮件内容
     * @param mimetype 内容类型 默认为text/plain,如果要发送HTML内容,应设置为text/html
     */
    public static void send(String[] receivers, String subject, String mailContent, String mimetype,EmailConfig emailConfig) {
    	send(receivers, subject, mailContent, null, mimetype,emailConfig);
    }
    /**
     * 发送邮件
     * @param receivers 收件人
     * @param subject 标题
     * @param mailContent 邮件内容
     * @param attachements 附件
     * @param mimetype 内容类型 默认为text/plain,如果要发送HTML内容,应设置为text/html
     */
    public static void send(String[] receivers, String subject, String mailContent, File[] attachements, String mimetype,EmailConfig emailConfig) {
        Properties props = new Properties();
        props.put("mail.smtp.host", emailConfig.getHost());//Smtp服务器地址
        props.put("mail.smtp.auth", emailConfig.getAuth());//需要校验
        props.put( "mail.transpost.protocol",emailConfig.getProtocol());
        props.put( "mail.smtp.port",emailConfig.getPort()); 
        
        Session session = Session.getDefaultInstance(props, new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
            	   return new PasswordAuthentication("18055563925@163.com","201314...");//登录用户名/密码
            }
        });
        session.setDebug(true);
        try {
            MimeMessage mimeMessage = new MimeMessage(session);
            mimeMessage.setFrom(new InternetAddress("18055563925@163.com"));//发件人邮件

            InternetAddress[] toAddress = new InternetAddress[receivers.length];
            for (int i=0; i<receivers.length; i++) {
                toAddress[i] = new InternetAddress(receivers[i]);
            }
            mimeMessage.setRecipients(Message.RecipientType.TO, toAddress);//收件人邮件
            mimeMessage.setSubject(subject, charset);
           mimeMessage.setSender(new InternetAddress("18055563925@163.com"));
         mimeMessage.setFrom(new InternetAddress("18055563925@163.com"));

        

         mimeMessage.setSubject(subject);

         mimeMessage.setHeader("Content-Transfer-Encoding", "7bit");
         SimpleDateFormat format = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z");
mimeMessage.setHeader("Date", format.format(new Date()));

            
            Multipart multipart = new MimeMultipart();
            //正文
            MimeBodyPart body = new MimeBodyPart();
           // body.setText(message, charset);不支持html
            body.setContent(mailContent, (mimetype!=null && !"".equals(mimetype) ? mimetype : defaultMimetype)+ ";charset="+ charset);
            multipart.addBodyPart(body);//发件内容
            //附件
            if(attachements!=null){
	            for (File attachement : attachements) {
	                MimeBodyPart attache = new MimeBodyPart();
	               //ByteArrayDataSource bads = new ByteArrayDataSource(byte[],"application/x-any");
	                attache.setDataHandler(new DataHandler(new FileDataSource(attachement)));
	                String fileName = getLastName(attachement.getName());
	                attache.setFileName(MimeUtility.encodeText(fileName, charset, null));
	                multipart.addBodyPart(attache);
	            }
            }
            mimeMessage.setContent(multipart);
           // SimpleDateFormat formcat = new SimpleDateFormat("yyyy-MM-dd");            
            mimeMessage.setSentDate(new Date());//formcat.parse("2010-5-23")
            Transport.send(mimeMessage);            
        } catch (Exception e) {
        	e.printStackTrace();
        }
    }

    private static String getLastName(String fileName) {
        int pos = fileName.lastIndexOf("\\");
        if (pos > -1) {
            fileName = fileName.substring(pos + 1);
        }
        pos = fileName.lastIndexOf("/");
        if (pos > -1) {
            fileName = fileName.substring(pos + 1);
        }
        return fileName;
    }
}

package com.erp.Resource;

public interface SomeStatic {
	public final String RESOURCE_CLASS = "com.erp.Resource.Resouce";
	public final String ROLE_MAP = "rolemap";
	public static final String VALIDATE_CODE = "validateCode";
	public static final String USERNAME = "userName";
	public static final String PASSWORD = "passWord";
	public static final String USER_TYPE = "type";
	public static final String FLOW_MAIN = "flowMain";
	public static final String FLOW = "flow";
	public static final String MSG = "msg";
	public static final String FLOW_MAP = "flowMap";
	public static final String FLOWMAIN_LIST = "flowMainList";
	/**
	 * some statics refer to the memecached
	 */
	public static final String SET_APP_KEY="setappkey";
	public static final String SEND_LIST="sendlist";
	public static final String EMAIL_CONFIG="emailconfig";
	public static final String USER_ID = "user_id";
	

}

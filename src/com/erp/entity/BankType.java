package com.erp.entity;

import com.erp.form.ErpObject;

/**
 * 银行类型    对应表tb_bank_type
 * 
 * @author ziyouzizai
 * @version 1.0
 */
@SuppressWarnings("serial")
public class BankType extends ErpObject implements java.io.Serializable {
	
	/**
	 * 银行类型ID
	 */
	private int id;
	
	/**
	 * 类型名称
	 */
	private String name;
	
	/**
	 * 银行类型状态
	 * 1：正常  2：异常
	 */
	private int status = -1;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
}

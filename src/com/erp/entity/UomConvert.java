package com.erp.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import com.erp.form.ErpObject;

@SuppressWarnings("serial")
public class UomConvert extends ErpObject implements Serializable {
    
    

    private int id;
    private int uom_id;
    private int c_uom_Id;
    private double convert;
    private Timestamp last_update;
    private int addUser_ID;
    private Timestamp create_date;
    
    public UomConvert(){
        
    }
    
    public UomConvert(int uom_id, int c_uom_Id) {
        this.uom_id = uom_id;
        this.c_uom_Id = c_uom_Id;
    }

    public UomConvert(int uom_id, int c_uom_Id, double convert, Timestamp last_update, int addUser_ID,
            Timestamp create_date) {
        super();
        this.uom_id = uom_id;
        this.c_uom_Id = c_uom_Id;
        this.convert = convert;
        this.last_update = last_update;
        this.addUser_ID = addUser_ID;
        this.create_date = create_date;
    }
    
    //调换uom_id和c_uom_Id的值
    public UomConvert reverse(){
        int temp = this.uom_id;
        this.uom_id=this.c_uom_Id;
        this.c_uom_Id=temp;
        return this;
    }


    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public int getUom_id() {
        return uom_id;
    }
    public void setUom_id(int uom_id) {
        this.uom_id = uom_id;
    }
    public int getC_uom_Id() {
        return c_uom_Id;
    }
    public void setC_uom_Id(int c_uom_Id) {
        this.c_uom_Id = c_uom_Id;
    }
    public double getConvert() {
        return convert;
    }
    public void setConvert(double convert) {
        this.convert = convert;
    }
    public Timestamp getLast_update() {
        return last_update;
    }
    public void setLast_update(Timestamp last_update) {
        this.last_update = last_update;
    }
    public int getAddUser_ID() {
        return addUser_ID;
    }
    public void setAddUser_ID(int addUser_ID) {
        this.addUser_ID = addUser_ID;
    }
    public Timestamp getCreate_date() {
        return create_date;
    }
    public void setCreate_date(Timestamp create_date) {
        this.create_date = create_date;
    }
    
    
    
}

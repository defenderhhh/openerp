package com.erp.entity;

import com.erp.form.ErpObject;

@SuppressWarnings("serial")
public class ClientType extends ErpObject implements java.io.Serializable{
    
    private int id;
    private String name;
    private String serial_no;
    private String english_name;
    private String description;
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getSerial_no() {
        return serial_no;
    }
    public void setSerial_no(String serial_no) {
        this.serial_no = serial_no;
    }
    public String getEnglish_name() {
        return english_name;
    }
    public void setEnglish_name(String english_name) {
        this.english_name = english_name;
    }
    
    
    
}

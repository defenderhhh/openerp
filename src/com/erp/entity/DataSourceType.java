package com.erp.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import com.erp.form.ErpObject;

@SuppressWarnings("serial")
public class DataSourceType extends ErpObject implements Serializable {

    private Timestamp lastUpdateDate;
    private Timestamp create_data;
    private int id;
    private int addUserId;
    private String description;
    public Timestamp getLastUpdateDate() {
        return lastUpdateDate;
    }
    public void setLastUpdateDate(Timestamp lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }
    public Timestamp getCreate_data() {
        return create_data;
    }
    public void setCreate_data(Timestamp create_data) {
        this.create_data = create_data;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public int getAddUserId() {
        return addUserId;
    }
    public void setAddUserId(int addUserId) {
        this.addUserId = addUserId;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    
    
}

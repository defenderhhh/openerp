/**
 * Created on Oct 21, 2013
 */
package com.erp.entity;

import java.sql.Timestamp;

import com.erp.form.ErpObject;

/**
 * @author 宝宝E世界  http://babyeworld.taobao.com
 * @version 1.0.0
 * 销售报价
 */
public class MarketPrice extends ErpObject implements java.io.Serializable {

    private static final long serialVersionUID = 8138008175926689813L;
    
    private int id;
    
    //含税金额（金额+税额）
    private double taxMoney;
    
    //单据日期
    private Timestamp quotationDate;
    
    //单据号码 uuid
    private String serialNo;
    
    //单别引用单据类别表的主键类型为idnumber（9）
    private int djlb;
    
    //送货地址
    private String address;
    
    //有效日期 (有效日期应大于单据日期)
    private Timestamp yxrq;
    
    //单价是否含税(从客户主文件的单价是否含税自动带出)
    private int taxType;
    
    //币种
    private int moneyType;
    
    //汇率
    private String convert;
    
    //业务人员Id
    private int taskUserId;
    
    //所属部门（业务人员的所属部门）
    private int roleId;
    
    //审核人Id
    private int addUserId;
    
    //状态销售报价单有五种状态 2成功，3作废，4删除，5审批中 6表示审核失败
    private int state;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getTaxMoney() {
        return taxMoney;
    }

    public void setTaxMoney(double taxMoney) {
        this.taxMoney = taxMoney;
    }

    public Timestamp getQuotationDate() {
        return quotationDate;
    }

    public void setQuotationDate(Timestamp quotationDate) {
        this.quotationDate = quotationDate;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public int getDjlb() {
        return djlb;
    }

    public void setDjlb(int djlb) {
        this.djlb = djlb;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Timestamp getYxrq() {
        return yxrq;
    }

    public void setYxrq(Timestamp yxrq) {
        this.yxrq = yxrq;
    }

    public int getTaxType() {
        return taxType;
    }

    public void setTaxType(int taxType) {
        this.taxType = taxType;
    }

    public int getMoneyType() {
        return moneyType;
    }

    public void setMoneyType(int moneyType) {
        this.moneyType = moneyType;
    }

    public String getConvert() {
        return convert;
    }

    public void setConvert(String convert) {
        this.convert = convert;
    }

    public int getTaskUserId() {
        return taskUserId;
    }

    public void setTaskUserId(int taskUserId) {
        this.taskUserId = taskUserId;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public int getAddUserId() {
        return addUserId;
    }

    public void setAddUserId(int addUserId) {
        this.addUserId = addUserId;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }
}
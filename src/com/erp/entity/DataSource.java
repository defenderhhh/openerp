package com.erp.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import com.erp.form.ErpObject;

@SuppressWarnings("serial")
public class DataSource extends ErpObject implements Serializable{
    
    private int id;
    private int dataSourceTypeId;
    private String description;
    private Timestamp lastUpdate;
    private Timestamp create_date;
    private int addUserId;
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public int getDataSourceTypeId() {
        return dataSourceTypeId;
    }
    public void setDataSourceTypeId(int dataSourceTypeId) {
        this.dataSourceTypeId = dataSourceTypeId;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public Timestamp getLastUpdate() {
        return lastUpdate;
    }
    public void setLastUpdate(Timestamp lastUpdate) {
        this.lastUpdate = lastUpdate;
    }
    
    public Timestamp getCreate_date() {
        return create_date;
    }
    public void setCreate_date(Timestamp create_date) {
        this.create_date = create_date;
    }
    public int getAddUserId() {
        return addUserId;
    }
    public void setAddUserId(int addUserId) {
        this.addUserId = addUserId;
    }
    
    
    
}

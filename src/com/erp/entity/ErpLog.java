package com.erp.entity;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
	/**
	 * 日志注解
	 * 以查询日志为例，只需在SystemLogController类的query方法上打上
	 * @ErpLog(description="日志查询")即可
	 * @author 李魏
	 */
	@Retention(RetentionPolicy.RUNTIME)
	// 注解的保留阶段：比如此是注解保留在”运行阶段“
	@Target(ElementType.METHOD)
	// 注解标注位置：此处只能是标注在方法上
	public @interface ErpLog {
		String description(); // 方法描述
	}

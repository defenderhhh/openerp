package com.erp.entity;

import java.io.Serializable;


@SuppressWarnings("serial")
public class EmailConfig implements Serializable{

       
    private static EmailConfig emailConfig;

    protected EmailConfig(){
        
    }
    
    public static EmailConfig getInstance(){
       if(emailConfig==null){
           emailConfig=new EmailConfig();
       }
       return emailConfig;
    }
    
    private String host;
    private String auth;
    private String protocol;
    private String port;
    public String getHost() {
        return host;
    }
    public void setHost(String host) {
        this.host = host;
    }
    public String getAuth() {
        return auth;
    }
    public void setAuth(String auth) {
        this.auth = auth;
    }
    public String getProtocol() {
        return protocol;
    }
    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }
    public String getPort() {
        return port;
    }
    public void setPort(String port) {
        this.port = port;
    }
    
    @Override
    public String toString(){
        StringBuilder accum = new StringBuilder();
        accum.append("host=");
        accum.append(host);
        accum.append(",port=");
        accum.append(port);
        accum.append(",protocol=");
        accum.append(protocol);
        accum.append(",auth=");
        accum.append(auth);
        return accum.toString();
    }
    
}

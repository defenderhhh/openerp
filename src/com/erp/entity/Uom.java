package com.erp.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import com.erp.form.ErpObject;

@SuppressWarnings("serial")
public class Uom extends ErpObject implements Serializable {

    private int id;
    private String name;
    private String description;
    private String abbreviation;
    private int addUser_id;
    private Timestamp last_update_date;
    private Timestamp create_date;
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
   
    public String getAbbreviation() {
        return abbreviation;
    }
    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }
    public int getAddUser_id() {
        return addUser_id;
    }
    public void setAddUser_id(int addUser_id) {
        this.addUser_id = addUser_id;
    }
    public Timestamp getLast_update_date() {
        return last_update_date;
    }
    public void setLast_update_date(Timestamp last_update_date) {
        this.last_update_date = last_update_date;
    }
    public Timestamp getCreate_date() {
        return create_date;
    }
    public void setCreate_date(Timestamp create_date) {
        this.create_date = create_date;
    }
    
    
    
    
}

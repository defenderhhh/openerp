package com.erp.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import com.erp.form.ErpObject;
/**
 * 
 * 功能名: saleTraceCode entity<br/>
 * 功能描述: saleTraceCode entity<br/>
 * Copyright: Copyright (c) 2012<br/>
 * 公司: 曙光云计算有限公司<br/>
 *
 * @author Jerry
 * @version 1.0.0
 */
public class SaleTraceCode extends ErpObject implements Serializable{
    
    private static final long serialVersionUID = 6335709164651317782L;
    
    private String marketCode;
    
    private String description;
    
    //type =1 是表示内部 type表示外部
    private int type;
    
    //最后一次更新时间
    private Timestamp lastUpdateDate;
    
    private Timestamp create_Date;
    
    private String dnis;
    
    private Timestamp beginDate;
    
    private Timestamp endDate;
    
    //billable lifetime
    private int countSceond;
    
    //traceable lifetime
    private int trackSecond;
    
    //销售活动id
    private int activerMarketId;

    public String getMarketCode() {
        return marketCode;
    }

    public void setMarketCode(String marketCode) {
        this.marketCode = marketCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Timestamp getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(Timestamp lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public String getDnis() {
        return dnis;
    }

    public void setDnis(String dnis) {
        this.dnis = dnis;
    }

    public Timestamp getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Timestamp beginDate) {
        this.beginDate = beginDate;
    }

    public Timestamp getEndDate() {
        return endDate;
    }

    public void setEndDate(Timestamp endDate) {
        this.endDate = endDate;
    }

    public int getCountSceond() {
        return countSceond;
    }

    public void setCountSceond(int countSceond) {
        this.countSceond = countSceond;
    }

    public int getTrackSecond() {
        return trackSecond;
    }

    public void setTrackSecond(int trackSecond) {
        this.trackSecond = trackSecond;
    }

    public int getActiverMarketId() {
        return activerMarketId;
    }

    public void setActiverMarketId(int activerMarketId) {
        this.activerMarketId = activerMarketId;
    }

	public Timestamp getCreate_Date() {
		return create_Date;
	}

	public void setCreate_Date(Timestamp create_Date) {
		this.create_Date = create_Date;
	}
}

/**
 * Created on Oct 21, 2013
 */
package com.erp.entity;

import com.erp.form.ErpObject;

/**
 * @author 宝宝E世界  http://babyeworld.taobao.com
 * @version 1.0.0
 * 销售报价明细
 */
public class MarketPriceDetail extends ErpObject implements java.io.Serializable {

    private static final long serialVersionUID = -4040001762189439069L;
    
    private int id;
    
    //物料编号
    private int wuLiaoId;
    
    //物料名称
    private String wuLiaoName;
    
    //物料型号
    private String ggxh;
    
    //单位名称
    private String danWeiName;
    
    //数量
    private int acount;
    
    //折前单价（先考虑与客户的最近交易价格，及最近与客户的最近报价记录，如果没有则根据那客户等级从物料主文件中自动带出）
    private double beforeDiscount;
    
    //折数（默认为100%可以修改）折数不能为负数
    private double discount;
    
    //单价（等于折扣前单价*折数）
    private double price;
    
    //金额（此金额为不含税金额）单价*数量
    private double money;
    
    //税率（默认位17%）
    private double tax;
    
    //本币金额：为不含税单价*数量（直接取交易单据表身分录的本币金额。）
    private double wlcount;
    
    //含税金额（金额+税额）
    private double taxMoney;
    
    //实际成本
    private double sjcb;
    
    //实际毛利
    private double sjml;
    
    //合计栏的实际毛利率(%)=实际毛利合计/本币金额合计*100%
    private double sjmll;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getWuLiaoId() {
        return wuLiaoId;
    }

    public void setWuLiaoId(int wuLiaoId) {
        this.wuLiaoId = wuLiaoId;
    }

    public String getWuLiaoName() {
        return wuLiaoName;
    }

    public void setWuLiaoName(String wuLiaoName) {
        this.wuLiaoName = wuLiaoName;
    }

    public String getGgxh() {
        return ggxh;
    }

    public void setGgxh(String ggxh) {
        this.ggxh = ggxh;
    }

    public String getDanWeiName() {
        return danWeiName;
    }

    public void setDanWeiName(String danWeiName) {
        this.danWeiName = danWeiName;
    }

    public int getAcount() {
        return acount;
    }

    public void setAcount(int acount) {
        this.acount = acount;
    }

    public double getBeforeDiscount() {
        return beforeDiscount;
    }

    public void setBeforeDiscount(double beforeDiscount) {
        this.beforeDiscount = beforeDiscount;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public double getTax() {
        return tax;
    }

    public void setTax(double tax) {
        this.tax = tax;
    }

    public double getWlcount() {
        return wlcount;
    }

    public void setWlcount(double wlcount) {
        this.wlcount = wlcount;
    }

    public double getTaxMoney() {
        return taxMoney;
    }

    public void setTaxMoney(double taxMoney) {
        this.taxMoney = taxMoney;
    }

    public double getSjcb() {
        return sjcb;
    }

    public void setSjcb(double sjcb) {
        this.sjcb = sjcb;
    }

    public double getSjml() {
        return sjml;
    }

    public void setSjml(double sjml) {
        this.sjml = sjml;
    }

    public double getSjmll() {
        return sjmll;
    }

    public void setSjmll(double sjmll) {
        this.sjmll = sjmll;
    }
}
package com.erp.entity;

import java.io.Serializable;
import java.util.Date;

import com.erp.form.ErpObject;

@SuppressWarnings("serial")
public class RequestBuy extends ErpObject implements Serializable {

    private int id;
    private String buyId;
    private int status;
    private String moneType;
    private int lastPrice;
    private String lastMoneyType;
    private String lastEndMoney;
    private Date requestDate;
    private int roleId;
    private int createUsers;
    private int okUser;
    private String serialNo;
    private int match;
    private String materSerialNo;
    private String norms;
    private String name;
    private int amount;
    private int okprice;
    private int money;
    private Date buyDate;
    private int type;
    private int market;
    private String materName;
    private int discount;
    
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getBuyId() {
        return buyId;
    }
    public void setBuyId(String buyId) {
        this.buyId = buyId;
    }
    public int getStatus() {
        return status;
    }
    public void setStatus(int status) {
        this.status = status;
    }
    public String getMoneType() {
        return moneType;
    }
    public void setMoneType(String moneType) {
        this.moneType = moneType;
    }
    public int getLastPrice() {
        return lastPrice;
    }
    public void setLastPrice(int lastPrice) {
        this.lastPrice = lastPrice;
    }
    public String getLastMoneyType() {
        return lastMoneyType;
    }
    public void setLastMoneyType(String lastMoneyType) {
        this.lastMoneyType = lastMoneyType;
    }
    public String getLastEndMoney() {
        return lastEndMoney;
    }
    public void setLastEndMoney(String lastEndMoney) {
        this.lastEndMoney = lastEndMoney;
    }
    public Date getRequestDate() {
        return requestDate;
    }
    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }
    public int getRoleId() {
        return roleId;
    }
    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }
    public int getCreateUsers() {
        return createUsers;
    }
    public void setCreateUsers(int createUsers) {
        this.createUsers = createUsers;
    }
    public int getOkUser() {
        return okUser;
    }
    public void setOkUser(int okUser) {
        this.okUser = okUser;
    }
    public String getSerialNo() {
        return serialNo;
    }
    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }
    public int getMatch() {
        return match;
    }
    public void setMatch(int match) {
        this.match = match;
    }
    public String getMaterSerialNo() {
        return materSerialNo;
    }
    public void setMaterSerialNo(String materSerialNo) {
        this.materSerialNo = materSerialNo;
    }
    public String getNorms() {
        return norms;
    }
    public void setNorms(String norms) {
        this.norms = norms;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getAmount() {
        return amount;
    }
    public void setAmount(int amount) {
        this.amount = amount;
    }
    public int getOkprice() {
        return okprice;
    }
    public void setOkprice(int okprice) {
        this.okprice = okprice;
    }
    public int getMoney() {
        return money;
    }
    public void setMoney(int money) {
        this.money = money;
    }
    public Date getBuyDate() {
        return buyDate;
    }
    public void setBuyDate(Date buyDate) {
        this.buyDate = buyDate;
    }
    public int getType() {
        return type;
    }
    public void setType(int type) {
        this.type = type;
    }
    public int getMarket() {
        return market;
    }
    public void setMarket(int market) {
        this.market = market;
    }
    public String getMaterName() {
        return materName;
    }
    public void setMaterName(String materName) {
        this.materName = materName;
    }
    public int getDiscount() {
        return discount;
    }
    public void setDiscount(int discount) {
        this.discount = discount;
    }
    
}

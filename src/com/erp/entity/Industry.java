package com.erp.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import com.erp.form.ErpObject;

@SuppressWarnings("serial")
public class Industry extends ErpObject implements Serializable{

    private int id;
    private String name;
    private String code;
    private Timestamp create_time;
    private int addUserId;
    private int type;
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }
    public Timestamp getCreate_time() {
        return create_time;
    }
    public void setCreate_time(Timestamp create_time) {
        this.create_time = create_time;
    }
    public int getAddUserId() {
        return addUserId;
    }
    public void setAddUserId(int addUserId) {
        this.addUserId = addUserId;
    }
    public int getType() {
        return type;
    }
    public void setType(int type) {
        this.type = type;
    }
    
    
    
    
}

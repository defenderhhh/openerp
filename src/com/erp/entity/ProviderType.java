package com.erp.entity;

import com.erp.form.ErpObject;

/**
 * 功能名: 供应商类别<br/>
 * 功能描述: 用于对供应商分类<br/>
 * Copyright: Copyright (c) 2013<br/>
 * 公司: xx有限公司<br/>
 *
 * @author liuhai
 * @version 1.0.0
 */
@SuppressWarnings("serial")
public class ProviderType extends ErpObject implements java.io.Serializable {
    private int id;
    private String name;
    private String ename;
    private String description;
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getEname() {
        return ename;
    }
    public void setEname(String ename) {
        this.ename = ename;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

}

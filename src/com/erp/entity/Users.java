package com.erp.entity;

import java.io.Serializable;
import java.util.List;

import com.erp.form.ErpObject;

@SuppressWarnings("serial")
public class Users extends ErpObject implements Serializable{
	private int id;//主键
	private String userName;//用户名
	private String passWord;
	private List<Roles> rolesList ;
	private String current;
	


	public String getCurrent() {
		return current;
	}


	public void setCurrent(String current) {
		this.current = current;
	}


	public List<Roles> getRolesList() {
		return rolesList;
	}


	public void setRolesList(List<Roles> rolesList) {
		this.rolesList = rolesList;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassWord() {
		return passWord;
	}
	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}
	
}
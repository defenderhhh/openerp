package com.erp.entity;

import java.sql.Timestamp;

import com.erp.form.ErpObject;

@SuppressWarnings("serial")
public class Client extends ErpObject implements java.io.Serializable{

    
    private String name;
    private int id;
    private String serial_no;
    private String money_serial_no;
    private String english_name;
    private int client_type;
    private String area;
    private int user_id;
    private int connection_user_id;
    private String phone_one;
    private String phone_two;
    private String mobile_phone;
    private int bank_id;
    private int do_type;
    private int task_user_id;
    private String e_mail;
    private String web_url;
    private String phone_send;
    private Timestamp begin_market_date;
    private short discount;
    private Timestamp begin_market_back_date;
    private String market_grade;
    private Timestamp last_market_date;
    private short istak;
    private short isbig;
    private Timestamp create_date;
    private Timestamp end_date;
    private double money;
    private double my_pack_money;
    private String description;
    private Timestamp every_month_date;
    private short grade;
    private double begin_pre_money;
    private double begin_must_money;
    private double end_pre_money;
    private String address;
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getSerial_no() {
        return serial_no;
    }
    public void setSerial_no(String serial_no) {
        this.serial_no = serial_no;
    }
    public String getMoney_serial_no() {
        return money_serial_no;
    }
    public void setMoney_serial_no(String money_serial_no) {
        this.money_serial_no = money_serial_no;
    }
    public String getEnglish_name() {
        return english_name;
    }
    public void setEnglish_name(String english_name) {
        this.english_name = english_name;
    }
    public int getClient_type() {
        return client_type;
    }
    public void setClient_type(int client_type) {
        this.client_type = client_type;
    }
    public String getArea() {
        return area;
    }
    public void setArea(String area) {
        this.area = area;
    }
    public int getUser_id() {
        return user_id;
    }
    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }
    public int getConnection_user_id() {
        return connection_user_id;
    }
    public void setConnection_user_id(int connection_user_id) {
        this.connection_user_id = connection_user_id;
    }
    public String getPhone_one() {
        return phone_one;
    }
    public void setPhone_one(String phone_one) {
        this.phone_one = phone_one;
    }
    public String getPhone_two() {
        return phone_two;
    }
    public void setPhone_two(String phone_two) {
        this.phone_two = phone_two;
    }
    public String getMobile_phone() {
        return mobile_phone;
    }
    public void setMobile_phone(String mobile_phone) {
        this.mobile_phone = mobile_phone;
    }
    public int getBank_id() {
        return bank_id;
    }
    public void setBank_id(int bank_id) {
        this.bank_id = bank_id;
    }
    public int getDo_type() {
        return do_type;
    }
    public void setDo_type(int do_type) {
        this.do_type = do_type;
    }
    public int getTask_user_id() {
        return task_user_id;
    }
    public void setTask_user_id(int task_user_id) {
        this.task_user_id = task_user_id;
    }
    public String getE_mail() {
        return e_mail;
    }
    public void setE_mail(String e_mail) {
        this.e_mail = e_mail;
    }
    public String getWeb_url() {
        return web_url;
    }
    public void setWeb_url(String web_url) {
        this.web_url = web_url;
    }
    public String getPhone_send() {
        return phone_send;
    }
    public void setPhone_send(String phone_send) {
        this.phone_send = phone_send;
    }
    public Timestamp getBegin_market_date() {
        return begin_market_date;
    }
    public void setBegin_market_date(Timestamp begin_market_date) {
        this.begin_market_date = begin_market_date;
    }
    public short getDiscount() {
        return discount;
    }
    public void setDiscount(short discount) {
        this.discount = discount;
    }
    public Timestamp getBegin_market_back_date() {
        return begin_market_back_date;
    }
    public void setBegin_market_back_date(Timestamp begin_market_back_date) {
        this.begin_market_back_date = begin_market_back_date;
    }
    public String getMarket_grade() {
        return market_grade;
    }
    public void setMarket_grade(String market_grade) {
        this.market_grade = market_grade;
    }
    public Timestamp getLast_market_date() {
        return last_market_date;
    }
    public void setLast_market_date(Timestamp last_market_date) {
        this.last_market_date = last_market_date;
    }
    public short getIstak() {
        return istak;
    }
    public void setIstak(short istak) {
        this.istak = istak;
    }
    public short getIsbig() {
        return isbig;
    }
    public void setIsbig(short isbig) {
        this.isbig = isbig;
    }
    public Timestamp getCreate_date() {
        return create_date;
    }
    public void setCreate_date(Timestamp create_date) {
        this.create_date = create_date;
    }
    public Timestamp getEnd_date() {
        return end_date;
    }
    public void setEnd_date(Timestamp end_date) {
        this.end_date = end_date;
    }
    public double getMoney() {
        return money;
    }
    public void setMoney(double money) {
        this.money = money;
    }
    public double getMy_pack_money() {
        return my_pack_money;
    }
    public void setMy_pack_money(double my_pack_money) {
        this.my_pack_money = my_pack_money;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public Timestamp getEvery_month_date() {
        return every_month_date;
    }
    public void setEvery_month_date(Timestamp every_month_date) {
        this.every_month_date = every_month_date;
    }
    public short getGrade() {
        return grade;
    }
    public void setGrade(short grade) {
        this.grade = grade;
    }
    public double getBegin_pre_money() {
        return begin_pre_money;
    }
    public void setBegin_pre_money(double begin_pre_money) {
        this.begin_pre_money = begin_pre_money;
    }
    public double getBegin_must_money() {
        return begin_must_money;
    }
    public void setBegin_must_money(double begin_must_money) {
        this.begin_must_money = begin_must_money;
    }
    public double getEnd_pre_money() {
        return end_pre_money;
    }
    public void setEnd_pre_money(double end_pre_money) {
        this.end_pre_money = end_pre_money;
    }
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    
}

package com.erp.entity;

import java.io.Serializable;

import com.erp.form.ErpObject;

@SuppressWarnings("serial")
public class TeamRole extends ErpObject implements Serializable{

    private int id;
    private int team_id;
    private int user_id;
    private int role_id;
    private int role_parent_id;
    private String description;
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public int getTeam_id() {
        return team_id;
    }
    public void setTeam_id(int team_id) {
        this.team_id = team_id;
    }
    public int getUser_id() {
        return user_id;
    }
    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }
    public int getRole_id() {
        return role_id;
    }
    public void setRole_id(int role_id) {
        this.role_id = role_id;
    }
    public int getRole_parent_id() {
        return role_parent_id;
    }
    public void setRole_parent_id(int role_parent_id) {
        this.role_parent_id = role_parent_id;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    
    
    
}

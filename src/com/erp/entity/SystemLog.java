package com.erp.entity;

import com.erp.form.ErpObject;
/**
 * 日志类<br/>
 * @author 李魏
 * @version 1.0.0
 */
@SuppressWarnings("serial")
public class SystemLog extends ErpObject implements java.io.Serializable{
	/**
	 * 日志ID
	 */
	private String pk ;
	
	/**
	 * 当前用户ID
	 */
	private String pk_user ;
	
	/**
	 * 当前用户名称
	 */
	private String user_name ;
	
	/**
	 * 客户端ip
	 */
	private String enter_ip ;
	
	/**
	 * 进入功能模块时间
	 */
	private String enter_method_time ;
	
	/**
	 * 会话ID
	 */
	private String session_id ;
	
	/**
	 * 记录方法名
	 */
	private String method_info ;
	
	/**
	 * 方法描述
	 * @return
	 */
	private String description ;

	public String getPk() {
		return pk;
	}

	public void setPk(String pk) {
		this.pk = pk;
	}

	public String getPk_user() {
		return pk_user;
	}

	public void setPk_user(String pkUser) {
		pk_user = pkUser;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String userName) {
		user_name = userName;
	}

	public String getEnter_ip() {
		return enter_ip;
	}

	public void setEnter_ip(String enterIp) {
		enter_ip = enterIp;
	}

	public String getEnter_method_time() {
		return enter_method_time;
	}

	public void setEnter_method_time(String enterMethodTime) {
		enter_method_time = enterMethodTime;
	}

	public String getSession_id() {
		return session_id;
	}

	public void setSession_id(String sessionId) {
		session_id = sessionId;
	}

	public String getMethod_info() {
		return method_info;
	}

	public void setMethod_info(String methodInfo) {
		method_info = methodInfo;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	
	
}

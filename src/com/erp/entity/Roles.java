package com.erp.entity;

import java.io.Serializable;
import java.util.Date;

import com.erp.form.ErpObject;

@SuppressWarnings("serial")
public class Roles extends ErpObject implements Serializable{

	private int id;//主键
	private String name;//角色名字
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	
}

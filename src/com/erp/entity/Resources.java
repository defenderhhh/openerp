package com.erp.entity;

import java.util.HashSet;
import java.util.Set;

import com.erp.form.ErpObject;

/**
 * Resources entity. @author MyEclipse Persistence Tools
 */

@SuppressWarnings({"serial","rawtypes"})
public class Resources  extends ErpObject implements java.io.Serializable  {

	private String memo;
	
	private String url;
	
	private Integer priority;
	

	private String name;
	
	private String codename;
	
	
	private Integer sort;

	
	private Set rolesResourceses = new HashSet(0);
	private Set gradeTypes = new HashSet(0);
	Set menuResources = new HashSet(0);

	// ConstructorsCS

	public Set getMenuResources() {
		return menuResources;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}


	public void setMenuResources(Set menuResources) {
		this.menuResources = menuResources;
	}

	/** default constructor */
	public Resources() {
	}

	/** full constructor */
	public Resources(String memo, String url, Integer priority, Integer type,
			String name, Set rolesResourceses, Set gradeTypes) {
		this.memo = memo;
		this.url = url;
		this.priority = priority;
		super.setType(type);
		this.name = name;
		this.rolesResourceses = rolesResourceses;
		this.gradeTypes = gradeTypes;
	}


	public String getCodename() {
		return codename;
	}

	public void setCodename(String codename) {
		this.codename = codename;
	}


	public String getMemo() {
		return this.memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Integer getPriority() {
		return this.priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}


	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set getRolesResourceses() {
		return this.rolesResourceses;
	}

	public void setRolesResourceses(Set rolesResourceses) {
		this.rolesResourceses = rolesResourceses;
	}

	public Set getGradeTypes() {
		return this.gradeTypes;
	}

	public void setGradeTypes(Set gradeTypes) {
		this.gradeTypes = gradeTypes;
	}

}